import os

import flask

from automata_builder import global_vars
from automata_builder.models import base
from automata_builder.models import model_exceptions
from automata_builder.models import regular_expression
from automata_builder.generator import graph_generator
from automata_builder.parser import automata_parser, parser_exceptions
from automata_builder.parser import regular_expression_parser


PARSER_APP = flask.Flask(__name__)
PARSER_APP.config.update(
    TESTING=True,
    SECRET_KEY=b'm2+[}!>/6CG7a5$27Fh9_',
    DEBUG=True,
)


@PARSER_APP.route('/', methods=['GET', 'POST'])
def render():
    if flask.request.method == 'GET':
        return flask.render_template('mainpage.html')
    else:
        textInput = flask.request.form['inputTextArea']
        regexExpressionInput = flask.request.form['regexExpressionInput']
        evaluation_strings = None
        automaton = None
        all_words = []
        try:
            if textInput:
                automaton = automata_parser.parse_automata_from_string(textInput)
                evaluation_strings = automaton.evaluate_input_strings()
            elif regexExpressionInput:
                root_component =\
                    regular_expression_parser.parse_and_build_regular_expression(
                        regexExpressionInput)
                regularExpression = regular_expression.RegularExpression(
                    root_component)
                automaton = regularExpression.get_automaton_transformation()

            if automaton:
                all_words = automaton.get_all_words()
                if all_words == NotImplemented:
                    all_words = []
                graph_generator.generate_graph_image_for_automaton(automaton)
                graph_generator.generate_graph_file_for_automaton(automaton)
                if(type(automaton) is base.NFA):
                   converted_nfa = automaton.\
                        get_dfa_conversion_using_subset_construction()
                   graph_generator.generate_graph_image_for_automaton(
                       converted_nfa)
                   graph_generator.generate_graph_file_for_automaton(converted_nfa)

        except parser_exceptions.ParserException as exc:
            flask.flash(exc.message)
        return flask.render_template(
            'mainpage.html', textInput=textInput, automaton=automaton,
            evaluation_strings=evaluation_strings,
            regexExpression=regexExpressionInput,
            all_words=all_words)


@PARSER_APP.route('/api/downloadautomaton/<id>', methods=['GET'])
def download_automaton_file(id):
    file = os.path.join(global_vars.AUTOMATA_FILES_DIR, f'generated_{id}.aut')
    if not os.path.exists(file):
        return flask.abort(400)
    return flask.send_file(file, as_attachment=True)
