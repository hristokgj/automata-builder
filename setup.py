from distutils.core import setup

setup(
    name='automata-builder',
    version='0.1dev',
    packages=['web_app', 'automata_builder', 'tests'],
    install_requires=[
        'graphviz==0.9',
        'pydot==1.2.4',
        'Flask==1.0.2',
        'pytest==3.8.2',
        'pytest-cov==2.6.0'
    ]
)
