"""Holds test for the automata_builder.graph module"""

from automata_builder import global_vars
from automata_builder.generator import graph_generator
from automata_builder.models import base

import os


def test_graph_image_for_automaton_is_generated():

    automaton = base.AutomatonBase()
    automaton.add_alphabet(set(['a']))
    state_A = base.State('A')
    state_B = base.State('B', is_final=True)
    automaton.add_state(state_A, initial=True)
    automaton.add_state(state_B, initial=True)


    t = base.Transition(state_A, state_B, 'a')
    automaton.add_transition(t)

    graph_generator.generate_graph_image_for_automaton(automaton)

    expected_path = os.path.join(global_vars.AUTOMATA_IMAGES_DIR, automaton.id)
    assert os.path.isfile(expected_path)
