"""Holds test for the automata_builder.parser module"""
import os

import pytest

from automata_builder import global_vars
from automata_builder.models import base
from automata_builder.models import regular_expression
from automata_builder.parser import automata_parser
from automata_builder.parser import regular_expression_parser
from automata_builder.parser import parser_exceptions


def setup_module(module):
    if not os.path.exists(global_vars.AUTOMATA_FILES_DIR):
        os.makedirs(global_vars.AUTOMATA_FILES_DIR)


def test_parse_automata_alphabet_from_string_list():
    """
    Test the parse_automata_alphabet_from_string_list method.
    """
    test_data = [
        {
            'expected_alphabet': None,
            'string_list': [
                'alphabetabcd'
            ],
            'expected_exception': parser_exceptions.ParserException
        },
        {
            'expected_alphabet': None,
            'string_list': [
                'not_an_alphabet: abcd'
            ],
            'expected_exception': parser_exceptions.ParserException
        },
        {
            'expected_alphabet': None,
            'string_list': [
                'alphabet:       '
            ],
            'expected_exception': parser_exceptions.ParserException
        },
        {
            'expected_alphabet': ['a', 'b', 'c', 'd'],
            'string_list': [
                'Alphabet:abcd'
            ],
            'expected_exception': None
        },
        {
            'expected_alphabet': ['a', 'b', 'c', 'd'],
            'string_list': [
                'alphabet:abcd'
            ],
            'expected_exception': None
        }
    ]

    for entry in test_data:
        automaton = base.AutomatonBase()
        if entry.get('expected_exception'):
            with pytest.raises(entry['expected_exception']):
                automata_parser.parse_automata_alphabet_from_string_list(
                    entry['string_list'], automaton)
        else:
            automata_parser.parse_automata_alphabet_from_string_list(
                entry['string_list'], automaton)
            assert automaton.alphabet == set(entry['expected_alphabet'])


def test_parse_automata_states_from_string_list():
    """
    Test the parse_automata_states_from_string_list.
    """
    test_data = [
        {
            'expected_states': [
                {'name': 'S', 'is_final': False, 'is_initial': True},
                {'name': 'A', 'is_final': False, 'is_initial': False},
                {'name': 'B', 'is_final': True, 'is_initial': False}
            ],
            'string_list': [
                'states: S, A, B',
                'final: B'
            ],
            'expected_exception': None
        },
        # states not defined
        {
            'expected_states': None,
            'string_list': [
                'final: B'
            ],
            'expected_exception': parser_exceptions.ParserException
        },
        # final not defined
        {
            'expected_states': None,
            'string_list': [
                'states: S, A, B'
            ],
            'expected_exception': parser_exceptions.ParserException
        },
        # Z defined as final but not defined in states
        {
            'expected_states': None,
            'string_list': [
                'states: S, A, B',
                'final: B, Z'
            ],
            'expected_exception': parser_exceptions.ParserException
        },
        # State defined with unallowed character - @
        {
            'expected_states': None,
            'string_list': [
                'states: @, A, B',
                'final: B'
            ],
            'expected_exception': parser_exceptions.ParserException
        },
        # No states specified
        {
            'expected_states': None,
            'string_list': [
                'states:    ',
                'final: B'
            ],
            'expected_exception': parser_exceptions.ParserException
        },
        # State defined with unallowed character - 1
        {
            'expected_states': None,
            'string_list': [
                'states: E, A, 1',
                'final: B'
            ],
            'expected_exception': parser_exceptions.ParserException
        },
        # Case sensitivity and spaces
        {
            'expected_states': [
                {'name': 'S', 'is_final': False, 'is_initial': True},
                {'name': 'A', 'is_final': False, 'is_initial': False},
                {'name': 'B', 'is_final': True, 'is_initial': False}
            ],
            'string_list': [
                'States: S,       A, B',
                'fInal: B'
            ],
            'expected_exception': None
        },
        # One letter + Number cases
        {
            'expected_states': [
                {'name': 'A0', 'is_final': False, 'is_initial': True},
                {'name': 'A1', 'is_final': False, 'is_initial': False},
                {'name': 'A2', 'is_final': True, 'is_initial': False}
            ],
            'string_list': [
                'States: A0,A1,A2',
                'final: A2'
            ],
            'expected_exception': None
        },
        # One letter + Number cases specified in lower
        {
            'expected_states': [
                {'name': 'A0', 'is_final': False, 'is_initial': True},
                {'name': 'A1', 'is_final': False, 'is_initial': False},
                {'name': 'A2', 'is_final': True, 'is_initial': False}
            ],
            'string_list': [
                'States: a0,a1,a2',
                'final: a2'
            ],
            'expected_exception': None
        },
        # Multiple letter states
        {
            'expected_states': [
                {'name': 'A0Q2', 'is_final': False, 'is_initial': True},
                {'name': 'A1Q3', 'is_final': False, 'is_initial': False},
                {'name': 'A2', 'is_final': True, 'is_initial': False}
            ],
            'string_list': [
                'States: A0Q2,A1Q3,A2',
                'final: A2'
            ],
            'expected_exception': None
        }
    ]
    for i, entry in enumerate(test_data):
        try:
            automaton = base.AutomatonBase()
            if entry.get('expected_exception'):
                with pytest.raises(entry['expected_exception']):
                    automata_parser.parse_automata_states_from_string_list(
                        entry['string_list'], automaton)
            else:
                automata_parser.parse_automata_states_from_string_list(
                    entry['string_list'], automaton)
                for expected_state_entry in entry['expected_states']:
                    state = automaton.get_state(expected_state_entry['name'])
                    assert state
                    assert state.is_final == expected_state_entry['is_final']
                    if expected_state_entry['is_initial']:
                        assert automaton.initial_state == state
        except AssertionError as err:
            print(f'Failed at test entry: {i + 1}')
            raise err


def test_parse_automata_transitions_from_string_list():
    """
    Test the parse_automata_transitions_from_string_list method.
    """
    test_data = [
        {
            'alphabet': set(['a', 'b']),
            'states': [
                {'name': 'S', 'is_final': False, 'is_initial': True},
                {'name': 'A', 'is_final': False, 'is_initial': False},
                {'name': 'B', 'is_final': True, 'is_initial': False}
            ],
            'expected_transition_keys': [
                'S/a/A', 'S/b/B', 'S/_/B', 'B/b/B'
            ],
            'string_list': [
                'transitions:',
                'S, a - -> A',
                'S, b - -> B',
                'S, _ - -> B',
                'B, b - -> B',
                'end.'
            ],
            'expected_exception': None
        },
        # Transition with a symbol - z not present in the alphabet.
        {
            'alphabet': set(['a', 'b']),
            'states': [
                {'name': 'S', 'is_final': False, 'is_initial': True},
                {'name': 'A', 'is_final': False, 'is_initial': False},
                {'name': 'B', 'is_final': True, 'is_initial': False}
            ],
            'expected_transition_keys': None,
            'string_list': [
                'transitions:',
                'S, a - -> A',
                'S, b - -> B',
                'S, _ - -> B',
                'B, z - -> B',
                'end.'
            ],
            'expected_exception': parser_exceptions.ParserException
        },
        # Incorrect formatting - first transition
        {
            'alphabet': set(['a', 'b']),
            'states': [
                {'name': 'S', 'is_final': False, 'is_initial': True},
                {'name': 'A', 'is_final': False, 'is_initial': False},
                {'name': 'B', 'is_final': True, 'is_initial': False}
            ],
            'expected_transition_keys': None,
            'string_list': [
                'transitions:',
                'S, a - - A',
                'S, b - -> B',
                'S, _ - -> B',
                'B, b - -> B',
                'end.'
            ],
            'expected_exception': parser_exceptions.ParserException
        },
        # Incorrect formatting - 'transitions:' not defined
        {
            'alphabet': set(['a', 'b']),
            'states': [
                {'name': 'S', 'is_final': False, 'is_initial': True},
                {'name': 'A', 'is_final': False, 'is_initial': False},
                {'name': 'B', 'is_final': True, 'is_initial': False}
            ],
            'expected_transition_keys': None,
            'string_list': [
                'S, a - -> A',
                'S, b - -> B',
                'S, _ - -> B',
                'B, b - -> B',
                'end.'
            ],
            'expected_exception': parser_exceptions.ParserException
        },
        # Missing state - O
        {
            'alphabet': set(['a', 'b']),
            'states': [
                {'name': 'S', 'is_final': False, 'is_initial': True},
                {'name': 'A', 'is_final': False, 'is_initial': False},
                {'name': 'B', 'is_final': True, 'is_initial': False}
            ],
            'expected_transition_keys': None,
            'string_list': [
                'transitions:',
                'S, a - -> A',
                'O, b - -> B',
                'S, _ - -> B',
                'B, b - -> B',
                'end.'
            ],
            'expected_exception': parser_exceptions.ParserException
        }
    ]
    for i, entry in enumerate(test_data):
        try:
            automaton = base.AutomatonBase()
            if entry.get('expected_exception'):
                with pytest.raises(entry['expected_exception']):
                    automata_parser.parse_automata_transitions_from_string_list(
                        entry['string_list'], automaton)
            else:
                for state_entry in entry['states']:
                    state = base.State(
                        name=state_entry['name'], is_final=state_entry['is_final'])
                    automaton.add_state(state, state_entry['is_initial'])

                automaton.add_alphabet(entry['alphabet'])

                # Prepare
                automata_parser.parse_automata_transitions_from_string_list(
                    entry['string_list'], automaton)


                # Assert
                for expected_transition_key in entry['expected_transition_keys']:
                    assert automaton.get_transition(expected_transition_key)

        except AssertionError as err:
            print(f'Failed at test entry: {i + 1}')
            raise err


def test_parse_automata_from_string():
    test_data = [
        {
            'expected_alphabet': set(['a', 'b']),
            'expected_states': [
                {'name': 'Z', 'is_final': False, 'is_initial': True},
                {'name': 'A', 'is_final': True, 'is_initial': False},
                {'name': 'B', 'is_final': True, 'is_initial': False}
            ],
            'expected_transition_keys': [
                'Z/a/A', 'Z/b/B', 'Z/_/B', 'B/b/B'
            ],
            'string': (
                '# automaton for regular expression (a|b)* \n'
                'alphabet: ab \n'
                'states: Z, A, B \n'
                'final: A, B \n'
                'transitions: \n'
                'Z, a - -> A \n'
                'Z, b - -> B \n'
                'Z, _ - -> B \n'
                'B, b - -> B \n'
                'end. \n'
            ),
        }
    ]
    for entry in test_data:
        automaton = automata_parser.parse_automata_from_string(entry['string'])
        assert automaton.alphabet == entry['expected_alphabet']
        for expected_state_entry in entry['expected_states']:
            state = automaton.get_state(expected_state_entry['name'])
            assert state
            assert state.is_final == expected_state_entry['is_final']
            if expected_state_entry['is_initial']:
                assert automaton.initial_state == state
        for transition_key in entry['expected_transition_keys']:
            assert automaton.get_transition(transition_key)


def test_parse_automata_from_file():
    test_data = [
        {
            'expected_alphabet': set(['a', 'b']),
            'expected_states': [
                {'name': 'Z', 'is_final': False, 'is_initial': True},
                {'name': 'A', 'is_final': True, 'is_initial': False},
                {'name': 'B', 'is_final': True, 'is_initial': False}
            ],
            'expected_transition_keys': [
                'Z/a/A', 'Z/b/B', 'Z/_/B', 'B/b/B'
            ],
            'file_lines': [
                '# temporary file generated from tests \n',
                '# automaton for regular expression (a|b)* \n',
                'alphabet: ab \n',
                'states: Z, A, B \n',
                'final: A, B \n',
                'transitions: \n',
                'Z, a - -> A \n',
                'Z, b - -> B \n',
                'Z, _ - -> B \n',
                'B, b - -> B \n',
                'end. \n',
            ],
        }
    ]
    for entry in test_data:
        temp_file = os.path.join(global_vars.AUTOMATA_FILES_DIR,
                                 'test_automaton_file.aut')
        if os.path.exists(temp_file):
            os.remove(temp_file)

        try:
            with open(temp_file, mode='a') as f:
                f.writelines(entry['file_lines'])
            automaton = automata_parser.parse_automata_from_file(temp_file)
            assert automaton.alphabet == entry['expected_alphabet']
            for expected_state_entry in entry['expected_states']:
                state = automaton.get_state(expected_state_entry['name'])
                assert state
                assert state.is_final == expected_state_entry['is_final']
                if expected_state_entry['is_initial']:
                    assert automaton.initial_state == state
            for transition_key in entry['expected_transition_keys']:
                assert automaton.get_transition(transition_key)
        finally:
            # Remove temporary file.
            os.remove(temp_file)


def test_check_if_input_strings_exist():
    """
    Test the check_if_input_strings_exist method works as intended.

    Method returns true only if the 'words:' string is found within the list of
    strings. (It does not check if input strings have actually been formatted
    correctly. This is done when trying to parse them.)
    """
    test_data = [
        {
            'string_list': [
                '# automaton for regular expression (a|b)*',
                'alphabet: ab',
                'states: Z, A, B',
                'final: A, B',
                'transitions:',
                'Z, a - -> A',
                'Z, b - -> B',
                'Z, _ - -> B',
                'B, b - -> B',
                'end.'
            ],
            'expected_result': False
        },
        {
            'string_list': [
                '# automaton for regular expression (a|b)*',
                'alphabet: ab',
                'states: Z, A, B',
                'final: A, B',
                'transitions:',
                'Z, a - -> A',
                'Z, b - -> B',
                'Z, _ - -> B',
                'B, b - -> B',
                'end.',
                'words:'
            ],
            'expected_result': True
        },
    ]
    for entry in test_data:
        result = automata_parser.check_if_input_strings_exist(
            entry['string_list'])
        assert result == entry['expected_result']

def test_parse_input_strings_from_string_list():
    test_data = [
        {
            'expected_input_strings': [
                ('aaaaaa', True),
                ('bbaba', True),
                ('cacaca', False)
            ],
            'string_list': [
                'words:',
                'aaaaaa,y',
                'bbaba,y',
                'cacaca,n',
                'end.'
            ],
            'expected_exception': None
        },
        # Wrong notation for acceptance
        {
            'expected_input_strings': [],
            'string_list': [
                'words:',
                'aaaaaa,correct',
                'bbaba,y',
                'cacaca,n',
                'end.'
            ],
            'expected_exception': parser_exceptions.ParserException
        },
        # Wrong split symbol
        {
            'expected_input_strings': [],
            'string_list': [
                'words:',
                'aaaaaa;y',
                'bbaba;y',
                'cacaca;n',
                'end.'
            ],
            'expected_exception': parser_exceptions.ParserException
        }
    ]
    for i, entry in enumerate(test_data):
        try:
            automaton = base.AutomatonBase()
            if entry.get('expected_exception'):
                with pytest.raises(entry['expected_exception']):
                    automata_parser.parse_input_strings_from_string_list(
                        entry['string_list'], automaton)
            else:
                # Prepare
                automata_parser.parse_input_strings_from_string_list(
                    entry['string_list'], automaton)

                assert automaton.input_strings == entry['expected_input_strings']

        except AssertionError as err:
            print(f'Failed at test entry: {i + 1}')
            raise err


def test_the_automata_stack_alphabet_is_parsed_correctly():
    test_data = [
        {
            'expected_stack_alphabet': set(['x', 'y']),
            'string_list': [
                'stack: xy'
            ],
            'expected_exception': None
        },
        # Stack alphabet is not mandatory
        {
            'expected_stack_alphabet': None,
            'string_list': [
                'alphabet: abc'
            ],
            'expected_exception': None
        },
        {
            'expected_stack_alphabet': None,
            'string_list': [
                'stack: '
            ],
            'expected_exception': parser_exceptions.ParserException
        }
    ]
    for i, entry in enumerate(test_data):
        try:
            automaton = base.AutomatonBase()
            if entry.get('expected_exception'):
                with pytest.raises(entry['expected_exception']):
                    automata_parser.\
                        parse_automata_stack_alphabet_from_string_list_if_present(
                            entry['string_list'], automaton)
            else:
                # Prepare
                automata_parser.\
                    parse_automata_stack_alphabet_from_string_list_if_present(
                    entry['string_list'], automaton)

                assert automaton.stack_alphabet == entry['expected_stack_alphabet']

        except AssertionError as err:
            print(f'Failed at test entry: {i + 1}')
            raise err


def test_regular_expression_parser_get_inner_expressions():
    # NOTE: Inner expressions are stripped of spaces
    test_data = [
        {
            'expression': '*(a)',
            'expected_inner_expressions': ['a']
        },
        {
            'expression': '.(*(a), b)',
            'expected_inner_expressions': ['*(a)', 'b']
        },
        {
            'expression': '.(*(a), |(b, c))',
            'expected_inner_expressions': ['*(a)', '|(b,c)']
        },
        {
            'expression': '.(.(a, *(d)), |(b, c))',
            'expected_inner_expressions': ['.(a,*(d))', '|(b,c)']
        }
    ]
    for entry in test_data:
        actual_inner_expressions = regular_expression_parser.get_inner_expressions(
            entry['expression'])
        assert actual_inner_expressions == entry['expected_inner_expressions']


def test_parse_and_build_regular_expression():
    expression_a = '.(.(a, *(d)), |(_, c))'

    symbol_a = regular_expression.RegularExpressionSymbol('a')
    symbol_empty = regular_expression.EmptySymbol()
    symbol_c = regular_expression.RegularExpressionSymbol('c')
    symbol_d = regular_expression.RegularExpressionSymbol('d')

    expected_root_element_exp_a = regular_expression.ConnectorConcatenation()

    concat = regular_expression.ConnectorConcatenation()
    kleene_s = regular_expression.ConnectorKleeneStar()
    kleene_s.attach_child_components([symbol_d])
    concat.attach_child_components([symbol_a, kleene_s])

    union = regular_expression.ConnectorUnion()
    union.attach_child_components([symbol_empty, symbol_c])

    expected_root_element_exp_a.attach_child_components([concat, union])

    actual_root_element_exp_a =\
        regular_expression_parser.parse_and_build_regular_expression(expression_a)

    assert str(actual_root_element_exp_a) == str(expected_root_element_exp_a)
