"""Holds test for the automata_builder.models.regular_expression module"""

from automata_builder.models import base
from automata_builder.models import regular_expression


def test_symbol_automaton_conversion():
    symbol = regular_expression.RegularExpressionSymbol('a')
    automaton = symbol.convert_to_automaton()
    # There should be exactly 2 states with a transition with the 'a' symbol.
    # Make sure that the alphabet consists of 'a' only as well.
    assert len(automaton.states) == 2
    assert len(automaton.transitions) == 1

    final_state = list(automaton.final_states.values())[0]
    expected_transition_key = base.Transition.build_key(
        automaton.initial_state, final_state, symbol.character)

    assert expected_transition_key in automaton.transitions.keys()

    assert set([symbol.character]) == automaton.alphabet


def test_tau_symbol_automaton_conversion():
    symbol = regular_expression.EmptySymbol()
    automaton = symbol.convert_to_automaton()
    # There should be exactly 2 states with a transition with the '_' symbol.
    # Make sure that the alphabet is empty and the transition is_tau is True.
    assert len(automaton.states) == 2
    assert len(automaton.transitions) == 1

    final_state = list(automaton.final_states.values())[0]
    expected_transition_key = base.Transition.build_key(
        automaton.initial_state, final_state, symbol.character)

    assert expected_transition_key in automaton.transitions.keys()
    assert automaton.transitions[expected_transition_key].is_tau == True
    assert set() == automaton.alphabet


def test_concatenation_automaton_conversion():
    symbol_a = regular_expression.RegularExpressionSymbol('a')
    symbol_b = regular_expression.RegularExpressionSymbol('b')
    concatenation = regular_expression.ConnectorConcatenation()
    concatenation.attach_child_components([symbol_a, symbol_b])

    automaton = concatenation.convert_to_automaton()
    # Without merge (tau transition) there should be 4 states.
    # 3 transitions: one with 'a' symbol, one with 'b' symbol, and one Tau.
    # if states A and B are connected with the 'a' symbol A should be the
    # initial state of the automaton.
    # if states X and Y are connected with the 'b' symbol -> Y should be the
    # only final state of the automaton.
    # B and X should be connected by the tau transition
    # Alphabet should be: set([a, b])

    assert len(automaton.states) == 4
    assert len(automaton.transitions) == 3

    a_transition = None
    count_a_transitions = 0
    for transition in automaton.transitions.values():
        if transition.symbol == 'a':
            a_transition = transition
            count_a_transitions += 1

    assert count_a_transitions == 1

    assert automaton.initial_state == a_transition.source_state

    b_transition = None
    count_b_transitions = 0
    for transition in automaton.transitions.values():
        if transition.symbol == 'b':
            b_transition = transition
            count_b_transitions += 1

    assert count_b_transitions == 1

    automaton_final_state = list(automaton.final_states.values())[0]

    assert automaton_final_state == b_transition.destination_state

    tau_transition = None
    count_tau_transitions = 0
    for transition in automaton.transitions.values():
        if transition.is_tau:
            tau_transition = transition
            count_tau_transitions += 1

    assert count_tau_transitions == 1

    assert tau_transition.source_state == a_transition.destination_state
    assert tau_transition.destination_state == b_transition.source_state

    assert automaton.alphabet == set(['a', 'b'])


def test_union_automaton_conversion():
    symbol_a = regular_expression.RegularExpressionSymbol('a')
    symbol_b = regular_expression.RegularExpressionSymbol('b')
    union = regular_expression.ConnectorUnion()
    union.attach_child_components([symbol_a, symbol_b])

    automaton = union.convert_to_automaton()
    # The following should exist:
    # 6 states, 6 transitions
    # transition with 'a' symbol
    # transition with 'b' symbol
    # 4 tau transitions
    # initial state should have tau transitions to source states of 'a' and 'b'
    # transitions
    # destination states of 'a' and 'b' transitions should have tau transitions
    # to automaton final state
    # Alphabet should be set(['a', 'b'])

    assert len(automaton.states) == 6
    assert len(automaton.transitions) == 6

    a_transition = None
    count_a_transitions = 0
    for transition in automaton.transitions.values():
        if transition.symbol == 'a':
            a_transition = transition
            count_a_transitions += 1

    assert count_a_transitions == 1

    b_transition = None
    count_b_transitions = 0
    for transition in automaton.transitions.values():
        if transition.symbol == 'b':
            b_transition = transition
            count_b_transitions += 1

    assert count_b_transitions == 1

    count_tau_transitions = 0
    for transition in automaton.transitions.values():
        if transition.is_tau:
            count_tau_transitions += 1

    assert count_tau_transitions == 4

    final_state = list(automaton.final_states.values())[0]

    init_tau_a_key = base.Transition.build_key(
        automaton.initial_state, a_transition.source_state, '_')

    init_tau_b_key = base.Transition.build_key(
        automaton.initial_state, b_transition.source_state, '_')

    a_tau_final = base.Transition.build_key(
        a_transition.destination_state, final_state, '_')

    b_tau_final = base.Transition.build_key(
        b_transition.destination_state, final_state, '_')

    assert init_tau_a_key in automaton.transitions.keys()
    assert init_tau_b_key in automaton.transitions.keys()
    assert a_tau_final in automaton.transitions.keys()
    assert b_tau_final in automaton.transitions.keys()

    assert automaton.alphabet == set(['a', 'b'])


def test_kleene_star_automaton_conversion():
    symbol_a = regular_expression.RegularExpressionSymbol('a')
    kleene_star = regular_expression.ConnectorKleeneStar()
    kleene_star.attach_child_components([symbol_a])

    automaton = kleene_star.convert_to_automaton()

    # 4 states, 5 transitions
    # 4 tau, 1 'a' transition
    # initial state should have tau to source of 'a' transition and the final state
    # destination of 'a' should have tau transition to final state
    # destination of 'a' should have tau to source of 'a'
    # Alphabet should be set(['a'])

    assert len(automaton.states) == 4
    assert len(automaton.transitions) == 5

    a_transition = None
    count_a_transitions = 0
    for transition in automaton.transitions.values():
        if transition.symbol == 'a':
            a_transition = transition
            count_a_transitions += 1

    assert count_a_transitions == 1

    count_tau_transitions = 0
    for transition in automaton.transitions.values():
        if transition.is_tau:
            count_tau_transitions += 1

    assert count_tau_transitions == 4

    final_state = list(automaton.final_states.values())[0]

    init_tau_a_key = base.Transition.build_key(
        automaton.initial_state, a_transition.source_state, '_')

    init_tau_final_key = base.Transition.build_key(
        automaton.initial_state, final_state, '_')

    a_tau_final_key = base.Transition.build_key(
        a_transition.destination_state, final_state, '_')

    a_dest_tau_source_key = base.Transition.build_key(
        a_transition.destination_state, a_transition.source_state, '_')

    assert init_tau_a_key in automaton.transitions.keys()
    assert init_tau_final_key in automaton.transitions.keys()
    assert a_tau_final_key in automaton.transitions.keys()
    assert a_dest_tau_source_key in automaton.transitions.keys()

    assert automaton.alphabet == set(['a'])
