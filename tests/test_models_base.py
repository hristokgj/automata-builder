"""Holds test for the automata_builder.models.base module"""

import pytest

from automata_builder.models import base
from automata_builder.models import model_exceptions
from automata_builder.parser import automata_parser

test_state_1 = base.State('T1')
test_state_2 = base.State('T2')

NFA_EXAMPLE_1_STRING = (
    '# (a*+(ab)*)*   \n'
    '# Infinite - True\n'
    'alphabet: ab    \n'
    'states: S, P, X, Y, Q\n'
    'final: P, Q \n'
    'transitions:\n'
    'S, _ --> P\n'
    'S, _ --> X\n'
    'P, a --> P\n'
    'P, _ --> S\n'
    'X, _ --> S\n'
    'X, a --> Y\n'
    'Y, b --> Q\n'
    'Q, _ --> X\n'
    'end.\n'
    'words:\n'
    '_,y\n'
    'abababa,y\n'
    'aaaababaaaaaaba,y\n'
    'aaaaa,y\n'
    'aaaabbaaaaaaab,n\n'
    'aaabb,n\n'
    'b,n\n'
    'c,n\n'
    'ba,n\n'
    'bab,n\n'
    'end.\n'
)

NFA_EXAMPLE_2_STRING = (
    '# aa*b + aa* \n'
    '# Infinite - True\n'
    'alphabet: ab \n'
    'states: O, P, Q, R, S, T, U, V, W, X, S \n'
    'final: S, X \n'
    'transitions: \n'
    'O, _ --> P \n'
    'O, _ --> V \n'
    'P, _ --> Q \n'
    'Q, _ --> R \n'
    'R, a --> S \n'
    'R, _ --> T \n'
    'S, _ --> R \n'
    'T, _ --> U \n'
    'U, _ --> O \n'
    'V, a --> W \n'
    'W, b --> X \n'
    'end. \n'
    'words:\n'
    'aaaaaaaab,y\n'
    'ab,y\n'
    'aaaaa,y\n'
    'aaaabbaaaaaaab,n\n'
    'aaabb,n\n'
    'b,n\n'
    'c,n\n'
    'ba,n\n'
    'bab,n\n'
    'end.\n'
)

DFA_EXAMPLE_STRING = (
    '# DFA example - strings that end in c with letters abc\n'
    '# Infinite - True\n'
    'alphabet: abc\n'
    'states: Z, X, Y, O\n'
    'final: O\n'
    'transitions:\n'
    'Z, a - -> X\n'
    'Z, b - -> Y\n'
    'Z, c - -> O\n'
    'X, a - -> X\n'
    'X, b - -> Y\n'
    'X, c - -> O\n'
    'Y, a - -> X\n'
    'Y, b - -> Y\n'
    'Y, c - -> O\n'
    'O, a - -> X\n'
    'O, b - -> Y\n'
    'O, c - -> O\n'
    'end.\n'
    'words:\n'
    'aaabbbac,y\n'
    'abc,y\n'
    'c,y\n'
    'abccaabbabaabccbac,y\n'
    'abccccaabba,n\n'
    'ccccca,n\n'
    'b,n\n'
    'a,n\n'
    'ccb,n\n'
    'bab,n\n'
    'end.\n'
)

# Same as DFA_EXAMPLE_STRING, but 2 otgoing transitions for 'c' on state Z
NFA_EXAMPLE_3_STRING = (
    '# DFA example - strings that end in c with letters abc\n'
    '# Infinite - True\n'
    'alphabet: abc\n'
    'states: Z, X, Y, O\n'
    'final: O\n'
    'transitions:\n'
    'Z, a - -> X\n'
    'Z, b - -> Y\n'
    'Z, c - -> O\n'
    'Z, c - -> X\n'
    'X, a - -> X\n'
    'X, b - -> Y\n'
    'X, c - -> O\n'
    'Y, a - -> X\n'
    'Y, b - -> Y\n'
    'Y, c - -> O\n'
    'O, a - -> X\n'
    'O, b - -> Y\n'
    'O, c - -> O\n'
    'end.\n'
    'words:\n'
    'aaabbbac,y\n'
    'abc,y\n'
    'c,y\n'
    'abccaabbabaabccbac,y\n'
    'abccccaabba,n\n'
    'ccccca,n\n'
    'b,n\n'
    'a,n\n'
    'ccb,n\n'
    'bab,n\n'
    'end.\n'
)

NFA_FINITE_MANY_EMPTY_TRANSITIONS = (
    '# Infinite - False\n'
    'alphabet: a\n'
    'states: O, X, M, N, Y, P, Q, Z, R, S, A, B, C\n'
    'final: S\n'
    'transitions:\n'
    'O, _ - -> X\n'
    'X, _ - -> M\n'
    'M, _ - -> N\n'
    'N, _ - -> X\n'
    'X, _ - -> Y\n'
    'Y, _ - -> P\n'
    'P, _ - -> Q\n'
    'Q, _ - -> Y\n'
    'Y, _ - -> Z\n'
    'Z, _ - -> R\n'
    'Z, _ - -> O\n'
    'O, a - -> S\n'
    'S, _ - -> S\n'
    'S, _ - -> A\n'
    'A, _ - -> B\n'
    'B, _ - -> C\n'
    'C, _ - -> S\n'
    'end.\n'
    'words:\n'
    'a,y\n'
    'aa,n\n'
    'aaa,n\n'
    'end.\n'
)

PDA_EXAMPLE_1 = (
    'alphabet: abc\n'
    'stack: x\n'
    'states: S, B, C\n'
    'final: C\n'
    'transitions:\n'
    'S, a[_, x] - -> S\n'
    'S, _ - -> B\n'
    'B, b[_, x] - -> B\n'
    'B, _ - -> C\n'
    'C, c[x, _] - -> C\n'
    'end.\n'
    'dfa: n\n'
    'finite: n\n'
    'words:, y\n'
    'abcc, y\n'
    'aacc, y\n'
    'bbbccc, y\n'
    'aaabbcccc, n\n'
    'aabbccccc, n\n'
    'bbaccc, n\n'
    'aaaabbbacccccccc, n\n'
    'end.\n'
)

SIMPLE_FINITE_NFA_1 = (
    'alphabet: abx\n'
    'states: O, A, P, Q, B, X\n'
    'final: A, B\n'
    'transitions:\n'
    'O, a - -> A\n'
    'A, b - -> P\n'
    'B, b - -> P\n'
    'X, b - -> P\n'
    'O, b - -> B\n'
    'O, x - -> X\n'
    'A, a - -> Q\n'
    'end.\n'
    'words:\n'
    'a, y\n'
    'b, y\n'
    'x, n\n'
    'xb, n\n'
    'ab, n\n'
    'aa, n\n'
    'bb, n\n'
    'end.\n'
)

SIMPLE_FINITE_NFA_2 = (
    'alphabet: a\n'
    'states: O, A, P, Q, R, S, T\n'
    'final: A, S\n'
    'transitions:\n'
    'O, a - -> A\n'
    'O, _ - -> P\n'
    'Q, _ - -> P\n'
    'P, _ - -> R\n'
    'R, _ - -> T\n'
    'T, _ - -> Q\n'
    'T, _ - -> S\n'
    'end.\n'
    'words:, y\n'
    'a, y\n'
    'end.\n'
)


def test_transition_state_attachment():

    t = base.Transition(test_state_1, test_state_2, 'x')

    assert t.source_state == test_state_1
    assert t.destination_state == test_state_2
    assert t.key in t.source_state.transitions_outgoing
    assert t.key in t.destination_state.transitions_ingoing
    assert t.source_state.transitions_outgoing[t.key] == t
    assert t.destination_state.transitions_ingoing[t.key] == t


def test_transition_tau_status_set():

    t = base.Transition(test_state_1, test_state_2, '_')

    assert t.is_tau


def test_adding_stack_alphabet_with_tau_should_throw_model_exception():
    automaton = base.AutomatonBase()
    stack_alphabet = set(['e', '_'])
    with pytest.raises(model_exceptions.ModelException):
        automaton.add_stack_alphabet(stack_alphabet)


def test_adding_alphabet_with_tau_should_throw_model_exception():
    automaton = base.AutomatonBase()
    alphabet = set(['a', 'b', 'c', '_'])
    with pytest.raises(model_exceptions.ModelException):
        automaton.add_alphabet(alphabet)


def test_adding_duplicate_state_should_throw_model_exception():
    automaton = base.AutomatonBase()
    state = base.State('A')
    automaton.add_state(state)
    with pytest.raises(model_exceptions.ModelException):
        automaton.add_state(state)


def test_adding_transition_with_symbol_not_in_alphabet_raises_model_exception():
    automaton = base.AutomatonBase()
    alphabet = set(['a', 'b', 'c'])
    automaton.add_alphabet(alphabet)
    state_A = base.State('A')
    state_B = base.State('B')
    automaton.add_states([state_A, state_B])
    transition = base.Transition(state_A, state_B, 'v')
    with pytest.raises(model_exceptions.ModelException):
        automaton.add_transition(transition)


def test_adding_duplicate_transition_raises_model_exception():
    automaton = base.AutomatonBase()
    alphabet = set(['a', 'b', 'c'])
    automaton.add_alphabet(alphabet)
    state_A = base.State('A')
    state_B = base.State('B')
    automaton.add_states([state_A, state_B])
    transition = base.Transition(state_A, state_B, 'a')
    automaton.add_transition(transition)
    with pytest.raises(model_exceptions.ModelException):
        automaton.add_transition(transition)


def test_adding_normal_transition_for_automaton_with_stack_raises_model_exception():
    automaton = base.AutomatonBase()
    alphabet = set(['a', 'b', 'c'])
    automaton.add_alphabet(alphabet)
    automaton.add_stack_alphabet(set(['x']))
    state_A = base.State('A')
    state_B = base.State('B')
    automaton.add_states([state_A, state_B])
    transition = base.Transition(state_A, state_B, 'a')
    with pytest.raises(model_exceptions.ModelException):
        automaton.add_transition(transition)


def test_transition_stack_symbols_are_in_stack_alphabet():
    test_data = [
        {
            'stack_alphabet': set(['x', 'y']),
            'pushable_symbol': 'x',
            'consumable_symbol': 'y',
            'expected_are_in_alphabet': True
        },
        {
            'stack_alphabet': set(['x', 'y']),
            'pushable_symbol': 'x',
            'consumable_symbol': '_',
            'expected_are_in_alphabet': True
        },
        {
            'stack_alphabet': set(['x', 'y']),
            'pushable_symbol': '_',
            'consumable_symbol': '_',
            'expected_are_in_alphabet': True
        },
        {
            'stack_alphabet': set(['x', 'y']),
            'pushable_symbol': '_',
            'consumable_symbol': 'y',
            'expected_are_in_alphabet': True
        },
        {
            'stack_alphabet': set(['x', 'y']),
            'pushable_symbol': 'a',
            'consumable_symbol': '_',
            'expected_are_in_alphabet': False
        },
        {
            'stack_alphabet': set(['x', 'y']),
            'pushable_symbol': 'x',
            'consumable_symbol': 'b',
            'expected_are_in_alphabet': False
        }
    ]
    for entry in test_data:
        automaton = base.AutomatonBase()
        automaton.add_stack_alphabet(entry['stack_alphabet'])
        stack_t = base.StackTransition(
            base.State('A'), base.State('B'), 'a',
            entry['consumable_symbol'], entry['pushable_symbol'])
        actual_result = automaton.transition_stack_symbols_are_in_stack_alphabet(
            stack_t)

        assert actual_result == entry['expected_are_in_alphabet']


# rmex : raises model exception
def test_adding_stack_transition_with_symbols_not_in_stack_alphabet_rmex():
    automaton = base.AutomatonBase()
    alphabet = set(['a', 'b', 'c'])
    automaton.add_alphabet(alphabet)
    automaton.add_stack_alphabet(set(['x']))
    state_A = base.State('A')
    state_B = base.State('B')
    automaton.add_states([state_A, state_B])
    transition = base.StackTransition(state_A, state_B, 'a', 'a', 'x')
    with pytest.raises(model_exceptions.ModelException):
        automaton.add_transition(transition)


def test_adding_stack_transition_with_missing_stack_alphabet_rmex():
    automaton = base.AutomatonBase()
    alphabet = set(['a', 'b', 'c'])
    automaton.add_alphabet(alphabet)
    state_A = base.State('A')
    state_B = base.State('B')
    automaton.add_states([state_A, state_B])
    transition = base.StackTransition(state_A, state_B, 'a', 'x', 'x')
    with pytest.raises(model_exceptions.ModelException):
        automaton.add_transition(transition)

def test_adding_methods_to_specific_automatons_raises_model_exception():
    # NFA
    specific_nfa_automaton = automata_parser.parse_automata_from_string(
        NFA_EXAMPLE_1_STRING)

    state_A = base.State('A')
    state_B = base.State('B', is_final=True)
    transition = base.Transition(state_A, state_B, 'a')

    new_state_C = base.State('C')
    with pytest.raises(model_exceptions.ModelException):
        specific_nfa_automaton.add_transition(transition)
    with pytest.raises(model_exceptions.ModelException):
        specific_nfa_automaton.add_state(new_state_C)
    with pytest.raises(model_exceptions.ModelException):
        specific_nfa_automaton.add_alphabet(['a', 'b'])


    # DFA
    specific_dfa_automaton = automata_parser.parse_automata_from_string(
        DFA_EXAMPLE_STRING)

    state_A = base.State('A')
    state_B = base.State('B', is_final=True)
    transition = base.Transition(state_A, state_B, 'a')

    new_state_C = base.State('C')
    with pytest.raises(model_exceptions.ModelException):
        specific_dfa_automaton.add_transition(transition)
    with pytest.raises(model_exceptions.ModelException):
        specific_dfa_automaton.add_state(new_state_C)
    with pytest.raises(model_exceptions.ModelException):
        specific_dfa_automaton.add_alphabet(['a', 'b'])

    # PDA
    specific_pda_automaton = automata_parser.parse_automata_from_string(
        PDA_EXAMPLE_1)

    state_A = base.State('A')
    state_B = base.State('B', is_final=True)
    transition = base.StackTransition(state_A, state_B, 'a', 'x', 'x')

    new_state_C = base.State('C')
    with pytest.raises(model_exceptions.ModelException):
        specific_pda_automaton.add_transition(transition)
    with pytest.raises(model_exceptions.ModelException):
         specific_pda_automaton.add_state(new_state_C)
    with pytest.raises(model_exceptions.ModelException):
        specific_pda_automaton.add_alphabet(['a', 'b'])
    with pytest.raises(model_exceptions.ModelException):
        specific_pda_automaton.add_stack_alphabet(['y'])


def test_input_evaluation_is_as_expected_for_the_given_words():
    test_data = [
        {
            'automaton_build_string': NFA_EXAMPLE_1_STRING
        },
        {
            'automaton_build_string': NFA_EXAMPLE_2_STRING
        },
        {
            'automaton_build_string': DFA_EXAMPLE_STRING
        },
        {
            'automaton_build_string': NFA_FINITE_MANY_EMPTY_TRANSITIONS
        },
        {
            'automaton_build_string': PDA_EXAMPLE_1
        },
        {
            'automaton_build_string': SIMPLE_FINITE_NFA_2
        }
    ]

    for i, entry in enumerate(test_data):
        automaton = automata_parser.parse_automata_from_string(
            entry['automaton_build_string'])

        evaluated_input_strings = automaton.evaluate_input_strings()
        for _, expected_result, actual_result in evaluated_input_strings:
            try:
                assert expected_result == actual_result
            except AssertionError as exc:
                print('Failed at entry: {i}')
                raise exc


def test_determine_type():
    """
    Test the determine_type method.
    """
    test_data = [
        {
            'automaton_build_string': DFA_EXAMPLE_STRING,
            'expected_type': base.DFA
        },
        {
            'automaton_build_string': NFA_EXAMPLE_1_STRING,
            'expected_type': base.NFA
        },
        {
            'automaton_build_string': NFA_EXAMPLE_2_STRING,
            'expected_type': base.NFA
        },
        {
            'automaton_build_string': NFA_EXAMPLE_3_STRING,
            'expected_type': base.NFA
        },
        {
            'automaton_build_string': NFA_FINITE_MANY_EMPTY_TRANSITIONS,
            'expected_type': base.NFA
        },
        {
            'automaton_build_string': PDA_EXAMPLE_1,
            'expected_type': base.PDA
        }
    ]
    for entry in test_data:
        automaton = automata_parser.parse_automata_from_string(
            entry['automaton_build_string'])

        assert type(automaton.determine_type()) is type(entry['expected_type'])


def test_nfa_to_dfa_conversion():
    nfa_example_1 = automata_parser.parse_automata_from_string(
        NFA_EXAMPLE_1_STRING)

    dfa_construction = nfa_example_1.get_dfa_conversion_using_subset_construction()

    # First check that it is actually a dfa
    assert type(dfa_construction) is base.DFA

    # Check if the alphabets are the same
    assert nfa_example_1.get_alphabet() == dfa_construction.get_alphabet()

    state_psx = dfa_construction.get_state('PSX')
    state_py = dfa_construction.get_state('PY')
    state_pqsx = dfa_construction.get_state('PQSX')
    state_sink = dfa_construction.get_state('SINK')

    assert state_psx
    assert state_py
    assert state_pqsx
    assert state_sink

    assert dfa_construction.initial_state == state_psx

    # all states containing Q or P should be end states
    assert state_psx.is_final
    assert state_py.is_final
    assert state_pqsx.is_final

    # check transitions

    # SINK transitions
    for symbol in dfa_construction.get_alphabet():
        key = base.Transition.build_key(state_sink, state_sink, symbol)
        t = state_sink.transitions_outgoing.get(key)
        assert t

    # psx transitions
    key = base.Transition.build_key(state_psx, state_py, 'a')
    t = state_psx.transitions_outgoing.get(key)
    assert t

    key = base.Transition.build_key(state_psx, state_sink, 'b')
    t = state_psx.transitions_outgoing.get(key)
    assert t

    # py transitions
    key = base.Transition.build_key(state_py, state_py, 'a')
    t = state_py.transitions_outgoing.get(key)
    assert t

    key = base.Transition.build_key(state_py, state_pqsx, 'b')
    t = state_py.transitions_outgoing.get(key)
    assert t

    # pqxs transitions
    key = base.Transition.build_key(state_pqsx, state_py, 'a')
    t = state_pqsx.transitions_outgoing.get(key)
    assert t

    key = base.Transition.build_key(state_pqsx, state_sink, 'b')
    t = state_pqsx.transitions_outgoing.get(key)
    assert t

    # finally assert that all given words for the original nfa are accepted or not
    # as expected
    for word, expected_status in nfa_example_1.input_strings:
        actual_status = dfa_construction.check_if_input_is_accepted(word)
        assert actual_status == expected_status

    # -------------------------------------------------------------------------------
    nfa_empty_transitions_finite = automata_parser.parse_automata_from_string(
        NFA_FINITE_MANY_EMPTY_TRANSITIONS)

    dfa_construction = nfa_empty_transitions_finite.\
        get_dfa_conversion_using_subset_construction()

    # First check that it is actually a dfa
    assert type(dfa_construction) is base.DFA

    # Check if the alphabets are the same
    assert(nfa_empty_transitions_finite.get_alphabet() ==
           dfa_construction.get_alphabet())

    state_mnopqrxyz = dfa_construction.get_state('MNOPQRXYZ')
    state_abcs = dfa_construction.get_state('ABCS')
    state_sink = dfa_construction.get_state('SINK')

    assert state_mnopqrxyz
    assert state_abcs
    assert state_sink

    assert dfa_construction.initial_state == state_mnopqrxyz

    # all states containing S should be end states
    assert state_abcs.is_final

    # check transitions

    # SINK transitions
    for symbol in dfa_construction.get_alphabet():
        key = base.Transition.build_key(state_sink, state_sink, symbol)
        t = state_sink.transitions_outgoing.get(key)
        assert t

    # mnopqrxyz transitions
    key = base.Transition.build_key(state_mnopqrxyz, state_abcs, 'a')
    t = state_mnopqrxyz.transitions_outgoing.get(key)
    assert t

    # abcs transitions
    key = base.Transition.build_key(state_abcs, state_sink, 'a')
    t = state_abcs.transitions_outgoing.get(key)
    assert t

    # finally assert that all given words for the original nfa are accepted or not
    # as expected
    for word, expected_status in nfa_empty_transitions_finite.input_strings:
        actual_status = dfa_construction.check_if_input_is_accepted(word)
        assert actual_status == expected_status


def test_infinite_status():
    """
    Check that the infinite status evaluation is as expected for DFA and NFA.

    Note: DFA is always infinite.
    """
    test_data = [
        {
            'automaton_build_string': DFA_EXAMPLE_STRING,
            'is_infinite': True
        },
        {
            'automaton_build_string': NFA_EXAMPLE_1_STRING,
            'is_infinite': True
        },
        {
            'automaton_build_string': NFA_EXAMPLE_2_STRING,
            'is_infinite': True
        },
        {
            'automaton_build_string': NFA_EXAMPLE_3_STRING,
            'is_infinite': True
        },
        {
            'automaton_build_string': NFA_FINITE_MANY_EMPTY_TRANSITIONS,
            'is_infinite': False
        },
        {
            'automaton_build_string': SIMPLE_FINITE_NFA_1,
            'is_infinite': False
        },
        {
            'automaton_build_string': SIMPLE_FINITE_NFA_2,
            'is_infinite': False
        }
    ]
    for entry in test_data:
        automaton = automata_parser.parse_automata_from_string(
            entry['automaton_build_string'])

        assert automaton.is_infinite() == entry['is_infinite']


def test_get_all_words():
    test_data = [
        {
            # Infinite
            'automaton_build_string': DFA_EXAMPLE_STRING,
            'expected_words': []
        },
        {
            # Infintie
            'automaton_build_string': NFA_EXAMPLE_1_STRING,
            'expected_words': []
        },
        {
            'automaton_build_string': NFA_FINITE_MANY_EMPTY_TRANSITIONS,
            'expected_words': ['a']
        },
        {
            'automaton_build_string': SIMPLE_FINITE_NFA_1,
            'expected_words': ['a', 'b', 'x', 'ab', 'bb', 'xb', 'aa']
        },
        {
            'automaton_build_string': SIMPLE_FINITE_NFA_2,
            'expected_words': ['a']
        }
    ]
    for entry in test_data:
        automaton = automata_parser.parse_automata_from_string(
            entry['automaton_build_string'])
        assert sorted(automaton.get_all_words()) == sorted(entry['expected_words'])


def test_automatic_name_generation_for_states_is_unique():
    """
    Assert that for multiple generated states, all of them have unique names.
    """
    names = set()
    for _ in range(0, 10):
        state = base.State(name=None, generate_state_name=True)
        if state.name in names:
            pytest.fail(f'Generated name:{state.name} is not unique.')
        names.add(state.name)

    assert True
