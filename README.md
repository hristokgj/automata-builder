# Automata Builder

Description
-----------

Web application built with Python/Flask for creating automatons from text input with specifc format or from regular expressions.

**Automaton text notation:**

![](https://gitlab.com/hristokgj/automata-builder/uploads/9766abd920dadd89c1021a806b53dad1/image.png)

**Regular expression notation:**

![](https://gitlab.com/hristokgj/automata-builder/uploads/aee87ccd27dd63f68952f98e22bb6949/image.png)


Functionality:
- Graph image generation
- File generation with defined format
- Finite status detection
- If finite: retrieval of all accepted words


Setup and Running
-----------------
**It is assumed that python version >= 3.5 is installed and path has been properly set so pip can be run.**

1. GraphViz (https://www.graphviz.org/) is **required**. Make sure that the bin folder has been
added to the PATH variable for the OS.

2. To setup the packages required for the project the following command can be run:
    ```shell
        pip install –e {path to project}
    ```
    It is highly recommended however to run the command in a virtual environment.
   
    To setup a preferable virtual environment: pipenv run the following command:
    ```shell
        pip install --user pipenv
    ```
    
    Then to setup the project using pipenv:
    ```shell
        pipenv sync
    ```
    
    To run commands within the environment use:
    ```shell
        pipenv run {command}
    ```

3. To run the project:
    ```shell
        python ./flask_application/launch.py
    ```
        

4. To run tests and coverage:
    ```shell
        pytest -v ./tests/
        pytest --cov=expression_parser ./tests/
    ```
    