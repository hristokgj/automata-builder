"""Module which holds global variables"""
import os

CURRENT_DIR = os.path.dirname(os.path.abspath(__file__))
PROJECT_DIR = os.path.abspath(os.path.join(CURRENT_DIR, os.pardir))
AUTOMATA_FILES_DIR = os.path.join(PROJECT_DIR, 'automatas')
APP_DIR = os.path.join(PROJECT_DIR, 'web_app')
STATIC_DIR = os.path.join(APP_DIR, 'static')
AUTOMATA_IMAGES_DIR = os.path.join(STATIC_DIR, 'img')
