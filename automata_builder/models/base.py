"""Module which holds base model abstract classes."""

import uuid

from automata_builder.models import model_exceptions as exc

TAU_SYMBOL = '_'

class State:
    _generated_names_counter = 0
    def __init__(self, name, is_final=False, generate_state_name=False):
        self.name = name
        if generate_state_name:
            self.name = State.generate_name()
        self.transitions_outgoing = {}
        self.transitions_ingoing = {}
        self.is_final = is_final

    @staticmethod
    def generate_name():
        name = f'Q{State._generated_names_counter}'
        State._generated_names_counter += 1
        return name

    def add_ingoing_transition(self, transition):
        """
        Attach an ingoing transition to the transitions_ingoing dict.
        """
        self.transitions_ingoing.update({transition.key : transition})

    def add_outgoing_transition(self, transition):
        """
        Attach an outgoing transition to the transitions_outgoing dict.
        """
        self.transitions_outgoing.update({transition.key : transition})


class Transition:

    def __init__(self, source_state, destination_state, symbol):
        self.source_state = source_state
        self.destination_state = destination_state
        self.symbol = symbol
        # Set tau tranisition status.
        self.is_tau = False
        if self.symbol == TAU_SYMBOL:
            self.is_tau = True
        self.key = Transition.build_key(source_state, destination_state, symbol)

        # Add self to both transition dictionaries
        self.source_state.add_outgoing_transition(self)
        self.destination_state.add_ingoing_transition(self)

    @staticmethod
    def build_key(source_state, destination_state, transition_letter):
        """
        Build a Transition key.

        Key is used to uniquely identify the Transition.

        """
        return f'{source_state.name}/{transition_letter}/{destination_state.name}'

    def __repr__(self):
        return (f'{self.source_state.name} '
                f'--<{self.symbol}>--> {self.destination_state.name}')

    def get_label(self):
        return self.symbol

    def get_text_file_representation(self):
        return (f'{self.source_state.name},{self.symbol} --> '
                f'{self.destination_state.name}\n')


class StackTransition(Transition):

    def __init__(self, source_state, destination_state, symbol,
                 consumable_stack_symbol, pushable_stack_symbol):
        super().__init__(source_state, destination_state, symbol)
        self.consumable_stack_symbol = consumable_stack_symbol
        self.pushable_stack_symbol = pushable_stack_symbol

        # Overwrite the base key to also take into consideration the stack

    @staticmethod
    def build_key(source_state, destination_state, transition_letter,
                  consumable_stack_symbol, pushable_stack_symbol):
        """
        Build a StackTransition key further identified by the stack symbols.

        Key is used to uniquely identify the StackTransition.

        """
        return (f'{source_state.name}/{transition_letter}/{consumable_stack_symbol}/'
                f'{pushable_stack_symbol}/{destination_state.name}')

    def __repr__(self):
        return (f'{self.source_state.name} '
                f'--<{self.symbol}>[{self.consumable_stack_symbol}, '
                f'{self.pushable_stack_symbol}]--> {self.destination_state.name}')

    def get_label(self):
        return (f'{self.symbol} [{self.consumable_stack_symbol},'
                f'{self.pushable_stack_symbol}]')

    def get_text_file_representation(self):

        if(self.consumable_stack_symbol == TAU_SYMBOL
           and self.pushable_stack_symbol == TAU_SYMBOL):
            return super().get_text_file_representation()

        return (f'{self.source_state.name},{self.symbol} '
                f'[{self.consumable_stack_symbol},{self.pushable_stack_symbol}]--> '
                f'{self.destination_state.name}\n')


class AutomatonBase():
    """
    Base mutable class for creating automatons and determining their type.
    """
    def __init__(self, alphabet=None, initial_state=None, states=None,
                 transitions=None, input_strings=None, stack_alphabet=None):
        self.alphabet = set()
        self.initial_state = None
        self.final_states = {}
        self.states = {}
        self.transitions = {}
        self.id = uuid.uuid4().hex
        self.input_strings = []
        self.mutable = True
        self.stack_alphabet = None

        if alphabet:
            self.add_alphabet(alphabet)

        self.has_stack = False
        if stack_alphabet:
            self.add_stack_alphabet(stack_alphabet)

        if states:
            for s in states:
                self.add_state(s)
            if not initial_state:
                raise exc.ModelException(
                    'Initial state must be given if states parameter is given.')
            self.initial_state = initial_state

        if input_strings:
            for input_string, expected_evaluation in input_strings:
                self.add_input_string(
                    input_string, expected_evaluation=expected_evaluation)

        if transitions:
            for t in transitions:
                self.add_transition(t)

    @staticmethod
    def concatenate_tuple(t):
        concatenated = ''
        for v in t:
            concatenated += v
        return concatenated

    def add_stack_alphabet(self, stack_alphabet):
        if not self.mutable:
            raise exc.ModelException(
                f'{type(self).__name__} specific implementation class is immutable.')

        for symbol in stack_alphabet:
            if symbol == TAU_SYMBOL:
                raise exc.ModelException(f'Symbol: {symbol} '
                                         f'is reserved for tau stack symbols'
                                         'and cannot be part of the stack alphabet.')
        self.has_stack = True
        self.stack_alphabet = stack_alphabet


    def add_alphabet(self, alphabet):
        if not self.mutable:
            raise exc.ModelException(
                f'{type(self).__name__} specific implementation class is immutable.')

        for symbol in alphabet:
            if symbol == TAU_SYMBOL:
                raise exc.ModelException(f'Symbol: {symbol} '
                                         f'is reserved for tau transitions'
                                         'and cannot be part of the alphabet.')
        self.alphabet = alphabet

    def get_alphabet(self):
        return self.alphabet

    def add_input_string(self, input_string, expected_evaluation=None):
        self.input_strings.append((input_string, expected_evaluation))

    def is_transition_symbol_in_alphabet(self, transition):
        if transition.is_tau:
            return True
        return transition.symbol in self.alphabet

    def add_state(self, state, initial=False):
        if not self.mutable:
            raise exc.ModelException(
                f'{type(self).__name__} specific implementation class is immutable.')
        if state.name in self.states:
            raise exc.ModelException(
                f'State:{state.name} is already defined within the automaton.')
        if initial:
            self.initial_state = state

        if state.is_final:
            self.final_states.update({state.name: state})

        self.states.update({state.name: state})

    def add_states(self, states):
        for s in states:
            self.add_state(s)

    def get_state(self, state_name):
        return self.states.get(state_name)

    def add_transition(self, transition):
        if not self.mutable:
            raise exc.ModelException(
                f'{type(self).__name__} specific implementation class is immutable.')
        if not self.is_transition_symbol_in_alphabet(transition):
            raise exc.ModelException(f'Symbol: {transition.symbol} '
                                     f'from transition: {transition} '
                                     'is not present within the alphabet.')

        if self.has_stack:
            if type(transition) is not StackTransition:
                raise exc.ModelException(
                    f'Cannot add non stack transition: {transition} to '
                    f'automaton with given stack alphabet')
            if not self.transition_stack_symbols_are_in_stack_alphabet(transition):
                raise exc.ModelException(
                    f'Stack symbols: [{transition.consumable_stack_symbol},'
                    f'{transition.pushable_stack_symbol}] '
                    f'from transition: {transition} are according to the '
                    f'stack alphabet: {self.stack_alphabet}.')

        elif type(transition) is StackTransition:
            raise exc.ModelException(
                f'Cannot add stack transition: {transition} to '
                f'automaton with missing stack alphabet.')

        if(transition.key in self.transitions):
            raise exc.ModelException(
                f'Transition:{transition.key} is already defined within '
                f'the automaton.')
        self.transitions.update({transition.key: transition})

    def transition_stack_symbols_are_in_stack_alphabet(
        self, transition:StackTransition):
        symbols_in_alphabet = True
        if(transition.pushable_stack_symbol != TAU_SYMBOL and
           transition.pushable_stack_symbol not in self.stack_alphabet):
           symbols_in_alphabet = False

        if(transition.consumable_stack_symbol != TAU_SYMBOL and
           transition.consumable_stack_symbol not in self.stack_alphabet):
           symbols_in_alphabet = False

        return symbols_in_alphabet

    def add_transitions(self, transitions):
        for t in transitions:
            self.add_transition(t)

    def get_transition(self, transition_key):
        return self.transitions.get(transition_key)

    def determine_type(self):
        """
        Determine the class type of the automaton based on the states/transitions.

        Returns
        -------
        <SpecificAutomaton> class implementation if type was determined else
        raises ModelException

        """
        # Currently only two options: NFA or DFA
        if AutomatonBase.is_dfa(self):
            return DFA
        elif self.has_stack and AutomatonBase.is_pda(self):
            return PDA
        elif not self.has_stack:
            return NFA

        raise exc.ModelException(
            "Could not determine specific automaton type based on attributes.")

    def construct_specific_automaton(self):
        """
        Constructs a specific Automaton object depending on the type.
        """
        specific_automaton_class = self.determine_type()

        return specific_automaton_class(
            self.alphabet, self.initial_state,
            self.states.values(), self.transitions.values(),
            self.input_strings, self.stack_alphabet)

    @staticmethod
    def is_dfa(automaton):
        """
        Determine if the automaton is a DFA.

        Assert that for each state:
        there is exactly one transition for each symbol of the alphabet.
        """
        for state in automaton.states.values():
            transition_symbols = set()

            for outgoing_t in state.transitions_outgoing.values():
                if outgoing_t.is_tau:
                    return False
                # More than one transition within the state for this symbol
                if outgoing_t.symbol in transition_symbols:
                    return False
                transition_symbols.add(outgoing_t.symbol)

            # Not all symbols have a transition
            if transition_symbols != automaton.alphabet:
                return False

        return True

    @staticmethod
    def is_pda(automaton):
        """
        Determine if the automaton is a PDA.

        Assert that all transitions are of type StackTransition.
        """
        for transition in automaton.transitions.values():
            if type(transition) is not StackTransition:
                return False
        return True


    def check_if_input_is_accepted(self, input_string):
        """
        Determine if the input string is accepted by the automaton.
        """
        return NotImplemented

    def get_all_words(self):
        """
        Determine if the input string is accepted by the automaton.
        """
        return NotImplemented


    def evaluate_input_strings(self):
        evaluated_input_strings = []
        for input_s, expected_evaluation in self.input_strings:
            result = self.check_if_input_is_accepted(input_s)
            evaluated_input_strings.append(
                (input_s, expected_evaluation, result))
        return evaluated_input_strings


    def is_infinite(self):
        return NotImplemented



class SpecificAutomaton(AutomatonBase):
    """
    Immutable class from which specific automaton implementations inherit.
    """

    def __init__(self, alphabet=None, initial_state=None, states=None,
                 transitions=None, input_strings=None, stack_alphabet=None):
        super().__init__(alphabet, initial_state, states,
                         transitions, input_strings, stack_alphabet)
        self.mutable = False


class NFA(SpecificAutomaton):
    def __init__(self, alphabet=None, initial_state=None, states=None,
                 transitions=None, input_strings=None, stack_alphabet=None):
        super().__init__(alphabet, initial_state, states,
                         transitions, input_strings, stack_alphabet)
        self.dfa_conversion = None

    def check_if_input_is_accepted(self, input_string):
        """
        Determine if the input string is accepted by the automaton.
        """
        input_string = ''.join(input_string.split())
        if not input_string:
            input_string = TAU_SYMBOL
        if input_string == TAU_SYMBOL and self.initial_state.is_final:
            return True
        input_list = list(input_string)
        starting_states = set([self.initial_state.name])
        input_exhausted_states = set()
        for i, symbol in enumerate(input_list):
            passed_states = set()
            all_reachable_states = set()
            for state_name in starting_states:
                state = self.states.get(state_name)
                reachable_states = self.get_reachable_states_from_symbol_for_state(
                    state, symbol, passed_states)
                all_reachable_states = all_reachable_states.union(reachable_states)
            # Input symbol cannot be processed
            if len(all_reachable_states) == 0:
                input_exhausted_states = all_reachable_states
                break
            starting_states = all_reachable_states

            # Input has been exhausted
            if i == len(input_list) - 1:
                input_exhausted_states = all_reachable_states

        for state_name in input_exhausted_states:
            state = self.states.get(state_name)
            if state.is_final:
                return True

        return False

    def get_dfa_conversion_using_subset_construction(self):
        construction_dict = {}
        states_to_process = set()
        _sink_state_name = 'SINK'

        n_initial_state_key = self.initial_state.name
        # Add any tau transition states to the new initial
        reachable_initial_states = self.get_reachable_states_from_symbol_for_state(
            self.initial_state, TAU_SYMBOL, stop_on_symbol_exhaustion=False)
        reachable_initial_states.add(self.initial_state.name)
        if len(reachable_initial_states) > 1:
            n_initial_state_key = tuple(sorted(
                s for s in reachable_initial_states))
        states_to_process.add(n_initial_state_key)

        while len(states_to_process) != 0:
            processed_state_keys = set()
            new_states = set()
            # Process each state and add any new states.
            for state_key in states_to_process:
                is_initial = True if state_key == n_initial_state_key else False
                construction_dict.update({state_key: {
                    'final': False, 'initial': is_initial, 'transitions': {}}})

                if state_key == _sink_state_name:
                    # If it's the sink state, add for each letter a transition
                    # to self.
                    for letter in self.alphabet:
                        construction_dict[state_key]['transitions'].update(
                            {letter: _sink_state_name})
                else:
                    # Update finite and initial status
                    state_names = list(state_key) if type(
                        state_key) is tuple else [state_key]
                    for state_name in state_names:
                        state = self.states[state_name]
                        if state.is_final:
                            construction_dict[state_key]['final'] = True

                    for letter in self.alphabet:
                        all_reachable_states = set()
                        for state_name in state_names:
                            state = self.states[state_name]
                            reachable_states =\
                                self.get_reachable_states_from_symbol_for_state(
                                    state, letter, stop_on_symbol_exhaustion=False)
                            all_reachable_states = all_reachable_states.union(
                                reachable_states)

                        # Add sink state if no reachable
                        if len(all_reachable_states) == 0:
                            all_reachable_states.add(_sink_state_name)

                        if len(all_reachable_states) > 1:
                            reachable_states_key = tuple(sorted(
                                s for s in all_reachable_states))
                        else:
                            reachable_states_key = list(all_reachable_states)[0]

                        construction_dict[state_key]['transitions'].update(
                            {letter: reachable_states_key})

                        # Add as new state to process if not present in the
                        # construction dictionary
                        if reachable_states_key not in construction_dict:
                            new_states.add(reachable_states_key)

                processed_state_keys.add(state_key)
            states_to_process = states_to_process - processed_state_keys
            states_to_process = states_to_process.union(new_states)

        # Replace tuple keys with strings
        converted_keys = {}
        for k, v in construction_dict.items():
            new_key = k
            if type(k) is tuple:
                new_key = AutomatonBase.concatenate_tuple(k)
            # Convert transition keys as well.
            converted_transitions = {}
            for symbol, dest_state_key in v['transitions'].items():
                new_dest_state_key = dest_state_key
                if type(dest_state_key) is tuple:
                    new_dest_state_key = AutomatonBase.concatenate_tuple(
                        dest_state_key)
                converted_transitions.update({symbol: new_dest_state_key})
            v['transitions'] = converted_transitions
            converted_keys.update({new_key: v})
        construction_dict = converted_keys

        converted_automaton = AutomatonBase(self.alphabet)
        for k, v in construction_dict.items():
            s = State(k, v['final'])
            converted_automaton.add_state(s, v['initial'])

        for k, v in construction_dict.items():
            source_state = converted_automaton.states[k]
            for letter, destination_state_name in v['transitions'].items():
                destination_state = converted_automaton.states[
                    destination_state_name]
                t = Transition(source_state, destination_state, letter)
                converted_automaton.add_transition(t)
        self.dfa_conversion = converted_automaton.construct_specific_automaton()
        return self.dfa_conversion

    def is_infinite(self):
        all_paths = self.get_all_transition_paths_from_state(self.initial_state)
        is_infinite = self.__check_if_transition_paths_form_non_empty_loop__(
            all_paths)
        return is_infinite

    def __check_if_transition_paths_form_non_empty_loop__(self, transition_paths):
        for path in transition_paths:
            transition_keys = path.split(',')
            passed_states = set()
            loop_start_state_name = None
            for i, key in enumerate(transition_keys):
                transition = self.transitions.get(key)
                # Self symbol loop
                if(not transition.is_tau and
                   transition.source_state.name ==
                   transition.destination_state.name):
                    return True

                # Then there is a loop and it starts at the first
                # transition whose destination state is the same as
                # the current transitions destination state.
                if(transition.destination_state.name in passed_states):
                    loop_start_state_name = transition.destination_state.name
                    break
                if(i == 0):
                    passed_states.add(transition.source_state.name)
                passed_states.add(transition.destination_state.name)

            if loop_start_state_name:
                loop_start_transition_key_index = 0
                for i, key in enumerate(transition_keys):
                    transition = self.transitions.get(key)
                    if transition.source_state.name == loop_start_state_name:
                        loop_start_transition_key_index = i
                        break

                for key in transition_keys[loop_start_transition_key_index:]:
                    transition = self.transitions.get(key)
                    # Loop with atleast one symbol to consumed.
                    if not transition.is_tau:
                        return True

        return False

    def get_all_words(self):
        if self.is_infinite():
            return []

        all_paths = self.get_all_transition_paths_from_state(self.initial_state)
        words = set()
        for path in all_paths:
            word = ''
            transition_keys = path.split(',')
            for key in transition_keys:
                transition = self.transitions.get(key)
                if not transition.is_tau:
                    word += transition.symbol
                # Add word when reaching destination state.
                if word:
                    words.add(word)

        return list(words)

    def get_reachable_states_from_symbol_for_state(
        self, startingState, symbol,
        passed_states=None, stop_on_symbol_exhaustion=True):
        # Only add to reachable if you can end up in the state by consuming the input
        # Tau transition states should not be considered as reachable if they do not
        # exhaust the input symbol.
        if not passed_states:
            passed_states = set()

        if startingState.name in passed_states:
            return set()

        passed_states.add(startingState.name)
        all_reachable_states = set()
        for transition in startingState.transitions_outgoing.values():
            # If the transition is equal to the symbol, then add the destination
            # name within the reachable states
            if transition.symbol == symbol:
                destination = transition.destination_state
                all_reachable_states.add(destination.name)
                reachable_states =\
                    self.get_reachable_states_from_symbol_for_state(
                        transition.destination_state,
                        TAU_SYMBOL, passed_states,
                        stop_on_symbol_exhaustion=stop_on_symbol_exhaustion)
                all_reachable_states = all_reachable_states.union(
                    reachable_states)
            # If it is a Tau transition, then recurse
            elif transition.is_tau:
                reachable_states = self.get_reachable_states_from_symbol_for_state(
                    transition.destination_state,
                    symbol, passed_states,
                    stop_on_symbol_exhaustion=stop_on_symbol_exhaustion)
                all_reachable_states = all_reachable_states.union(
                    reachable_states)

        return all_reachable_states

    def get_all_transition_paths_from_state(self, state, passed_states=None,
                                            current_path=None):
        if not passed_states:
            passed_states = set()
        all_paths = []
        if state.name in passed_states:
            return all_paths

        passed_states.add(state.name)
        for transition in state.transitions_outgoing.values():

            passed_states_copy = passed_states.copy()
            transition_path = f'{current_path},{transition.key}'
            if not current_path:
                transition_path = transition.key

            paths_from_t =\
                self.get_all_transition_paths_from_state(
                    transition.destination_state,
                    passed_states_copy, transition_path)

            if len(paths_from_t) == 0:
                all_paths.append(transition_path)

            for path in paths_from_t:
                all_paths.append(path)

        return all_paths

    def __repr__(self):
        return 'NFA'


class DFA(NFA):
    def __init__(self, alphabet=None, initial_state=None, states=None,
                 transitions=None, input_strings=None, stack_alphabet=None):
        super().__init__(alphabet, initial_state, states,
                         transitions, input_strings, stack_alphabet)
        if not AutomatonBase.is_dfa(self):
            raise exc.ModelException(
                'Cannot instantiate DFA, property not fulfilled.')

    def is_infinite(self):
        return True

    def __repr__(self):
        return 'DFA'


class PDA(SpecificAutomaton):

    def __init__(self, alphabet=None, initial_state=None, states=None,
                 transitions=None, input_strings=None, stack_alphabet=None):
        super().__init__(alphabet, initial_state, states,
                         transitions, input_strings, stack_alphabet)
        if not AutomatonBase.is_pda(self):
            raise exc.ModelException(
                'Cannot instantiate PDA, property not fulfilled.')

    def __repr__(self):
        return 'PDA'

    def check_if_input_is_accepted(self, input_string):
        """
        Determine if the input string is accepted by the automaton.
        """
        input_string = ''.join(input_string.split())
        if not input_string:
            input_string = TAU_SYMBOL
        if input_string == TAU_SYMBOL and self.initial_state.is_final:
            return True
        input_list = list(input_string)
        starting_state_key = self.create_stack_and_state_key(self.initial_state, [])
        starting_state_dict = { starting_state_key: {
            'state_name': self.initial_state.name, 'stack': []}}

        input_exhausted_states_dict = {}
        for i, symbol in enumerate(input_list):
            all_reachable_states_dict = {}
            for _, bundled_values_dict in starting_state_dict.items():
                state_name = bundled_values_dict['state_name']
                stack = bundled_values_dict['stack']
                state = self.states.get(state_name)
                reachable_states_dict =\
                    self.get_reachable_states_from_symbol_and_stack_for_state(
                        state, symbol, stack=stack)
                all_reachable_states_dict = {
                    **all_reachable_states_dict, **reachable_states_dict}
            # Input symbol cannot be processed
            if len(all_reachable_states_dict.keys()) == 0:
                input_exhausted_states_dict = all_reachable_states_dict.copy()
                break

            starting_state_dict = all_reachable_states_dict

            # Input has been exhausted
            if i == len(input_list) - 1:
                input_exhausted_states_dict = all_reachable_states_dict

        for _, bundled_values_dict in input_exhausted_states_dict.items():
            state_name = bundled_values_dict['state_name']
            stack = bundled_values_dict['stack']
            state = self.states.get(state_name)
            if state.is_final and len(stack) == 0:
                return True

        return False

    def get_reachable_states_from_symbol_and_stack_for_state(
            self, startingState: State, symbol,
            passed_states=None,  stack=None):
        key = self.create_stack_and_state_key(startingState, stack)
        if not passed_states:
            passed_states = set()

        if not stack:
            stack = []

        if key in passed_states:
            return {}

        passed_states.add(key)
        all_reachable_states_dict = {}
        for transition in startingState.transitions_outgoing.values():
            # If the transition is equal to the symbol, then add the destination
            # name within the reachable states
            if(transition.symbol == symbol and
               self.check_if_stack_transition_can_be_made(transition, stack)):
                destination_state = transition.destination_state
                updated_stack = self.pop_and_add_transition_symbols_to_stack_copy(
                    transition, stack)
                dest_key = self.create_stack_and_state_key(
                    destination_state, updated_stack)
                all_reachable_states_dict.update(
                    {dest_key: {
                        'state_name': destination_state.name,
                        'stack': updated_stack}})

                # If the symbol is empty then try to recurse further
                if symbol is TAU_SYMBOL:
                    reachable_states_dict =\
                        self.get_reachable_states_from_symbol_and_stack_for_state(
                            transition.destination_state,
                            symbol, passed_states, updated_stack)

                    all_reachable_states_dict = {
                        **all_reachable_states_dict, **reachable_states_dict}

            # If it is a Tau transition, then recurse
            elif(transition.is_tau
               and self.check_if_stack_transition_can_be_made(transition, stack)):
                updated_stack = self.pop_and_add_transition_symbols_to_stack_copy(
                    transition, stack)
                reachable_states_dict =\
                    self.get_reachable_states_from_symbol_and_stack_for_state(
                        transition.destination_state,
                        symbol, passed_states, updated_stack)

                all_reachable_states_dict = {
                    **all_reachable_states_dict, **reachable_states_dict}
        return all_reachable_states_dict

    def create_stack_and_state_key(self, state:State, stack:list):
        return f"{state.name}/{''.join(stack)}"

    def check_if_stack_transition_can_be_made(
        self, transition:StackTransition, stack:list):
        """
        Check if the transition can be made for the top element of the stack.
        """

        # Stack consumable is TAU
        if transition.consumable_stack_symbol == TAU_SYMBOL:
            return True

        # Symbols match
        if len(stack) > 0 and stack[-1] == transition.consumable_stack_symbol:
            return True

        return False

    def pop_and_add_transition_symbols_to_stack_copy(
        self, transition: StackTransition, stack:list):
        """
        Pop the top symbol and add the pushable symbol to a copy of the stack.

        """
        stack_copy = stack.copy()

        if transition.consumable_stack_symbol != TAU_SYMBOL:
            stack_copy.pop()

        if transition.pushable_stack_symbol != TAU_SYMBOL:
            stack_copy.append(transition.pushable_stack_symbol)

        return stack_copy
