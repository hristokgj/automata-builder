"""Module which holds models related to a regular expression."""
import abc

from automata_builder.models import base
from automata_builder.models import model_exceptions


class RegularExpressionComponent(abc.ABC):
    max_nr_related_components = 0

    def __init__(self, character):
        self.character = character
        self.related_components = []

    def attach_child_components(self, child_components):
        if len(child_components) != self.max_nr_related_components:
            raise model_exceptions.ModelException(
                f'Cannot attach child components to: {self.character}, '
                f'required number of components is {self.max_nr_related_components}, '
                f'while {len(child_components)} were given.')
        for c in child_components:
            self.related_components.append(c)

    @abc.abstractclassmethod
    def convert_to_automaton(self):
        raise model_exceptions.ModelException(
            f'convert_to_automaton is not implemented '
            f'for class:{self.__class__.__name__}')

    @abc.abstractclassmethod
    def transform(self, child_transformations):
        raise model_exceptions.ModelException(
            f'convert_to_automaton is not implemented '
            f'for class:{self.__class__.__name__}')


class RegularExpressionSymbol(RegularExpressionComponent):

    def attach_child_components(self, child_components):
        raise model_exceptions.ModelException(
            f'Cannot attach child components to RegularExpressionSymbol.')

    def convert_to_automaton(self):
        return self.transform()

    def __repr__(self):
        return self.character

    def transform(self):
        # Symbols are transformed by creating an initial and final state
        # with a transition whose symbol is equal to the character.
        new_init_state = base.State(None, generate_state_name=True)
        new_final_state = base.State(
            None, is_final=True, generate_state_name=True)

        transition = base.Transition(
            new_init_state, new_final_state, self.character)

        alphabet = set()
        alphabet.add(self.character)

        transformed_automaton = base.AutomatonBase(
            alphabet=alphabet, initial_state=new_init_state,
            states=[new_init_state, new_final_state], transitions=[transition])

        return transformed_automaton.construct_specific_automaton()


class EmptySymbol(RegularExpressionSymbol):
    CHARACTER = '_'
    def __init__(self, symbol=None):
        super().__init__(character=EmptySymbol.CHARACTER)

    def transform(self):
        # Same as a normal symbol transformation, however alphabet should be empty
        new_init_state = base.State(None, generate_state_name=True)
        new_final_state = base.State(
            None, is_final=True, generate_state_name=True)

        transition = base.Transition(
            new_init_state, new_final_state, self.character)

        transformed_automaton = base.AutomatonBase(initial_state=new_init_state,
            states=[new_init_state, new_final_state], transitions=[transition])

        return transformed_automaton.construct_specific_automaton()


class RegularExpressionConnector(RegularExpressionComponent):
    def __init__(self, character=None):
        character = INVERTED_CLASS_CONNECTOR_MAPPINGS.get(
            self.__class__.__name__)
        if not character:
            raise model_exceptions.ModelException(
                f'Connector character has not been added '
                f'within the mappings for:{self.__class__.__name__}')
        super().__init__(character=character)

    def __repr__(self):
        repr_string = f'{self.character}('
        for related_component in self.related_components:
            repr_string += f'{str(related_component)},'
        # remove last ','
        repr_string = repr_string[:-1]
        repr_string += f')'

        return repr_string

    def convert_to_automaton(self):
        child_transformations = []
        for component in self.related_components:
            child_transformations.append(component.convert_to_automaton())

        transformation = self.transform(child_transformations)

        return transformation

    @abc.abstractclassmethod
    def transform(self, child_transformations):
        raise model_exceptions.ModelException(
            f'convert_to_automaton is not implemented '
            f'for class:{self.__class__.__name__}')


class ConnectorConcatenation(RegularExpressionConnector):
    max_nr_related_components = 2

    def transform(self, child_transformations):
        """
        Transform the regular expression using the Thompson algorithm.

        Thompson transformation for concatenation of expressions A and B:
        transformed automaton has initial state of A.
        final state of A and initial state of B are merged with TAU transition.
        (final state of A is no longer final)
        final state of the transformed automaton becomes the final state of B.
        """
        left_component_automaton = child_transformations[0]
        right_component_automaton = child_transformations[1]

        # Create a merged alphabet of both automatons
        merged_alphabet = left_component_automaton.alphabet.union(
            right_component_automaton.alphabet)

        final_state_left = list(left_component_automaton.final_states.values())[0]
        if len(left_component_automaton.final_states.values()) > 1:
            raise model_exceptions.ModelException(
                'More than one final state found for Thompson transformation.')

        # Unset the is_final flag from the final state of the left automaton
        final_state_left.is_final = False
        # Merge the left and right automatons by a tau transition on the final
        # state of the left to the initial state of the right
        connection_transition = base.Transition(
            final_state_left, right_component_automaton.initial_state, '_')

        # Add all states from the left automaton and set the initial
        # state to be equal to that of the left automaton
        transformed_automaton = base.AutomatonBase(
            alphabet=merged_alphabet,
            initial_state=left_component_automaton.initial_state,
            states=left_component_automaton.states.values())

        # Add all states from the right automaton.
        transformed_automaton.add_states(right_component_automaton.states.values())

        # Merge the transition dictionaries of both automatons
        transformed_automaton.add_transitions(
            left_component_automaton.transitions.values())
        transformed_automaton.add_transitions(
            right_component_automaton.transitions.values())

        # Add the connection transition
        transformed_automaton.add_transition(connection_transition)

        return transformed_automaton.construct_specific_automaton()


class ConnectorUnion(RegularExpressionConnector):
    max_nr_related_components = 2

    def transform(self, child_transformations):
        """
        Transform the regular expression using the Thompson algorithm.

        Thompson transformation for union of expressions A and B:
        New initial state with TAU transitions to initial states of A and B.
        New final state with TAU transitions from A and B.
        (dont forget to unset final state status of A and B final states.)
        """

        left_component_automaton = child_transformations[0]
        right_component_automaton = child_transformations[1]
        transformed_automaton = base.AutomatonBase()

        # Add the merged alphabet of both automatons
        merged_alphabet = left_component_automaton.alphabet.union(
            right_component_automaton.alphabet)
        transformed_automaton.add_alphabet(merged_alphabet)

        new_init_state = base.State(None, generate_state_name=True)
        new_final_state = base.State(None, is_final=True, generate_state_name=True)

        final_state_left = list(left_component_automaton.final_states.values())[0]
        if len(left_component_automaton.final_states.values()) > 1:
            raise model_exceptions.ModelException(
                'More than one final state found for Thompson transformation.')

        final_state_right = list(right_component_automaton.final_states.values())[0]
        if len(transformed_automaton.final_states.values()) > 1:
            raise model_exceptions.ModelException(
                'More than one final state found for Thompson transformation.')

        # Unset is_final values
        final_state_left.is_final = False
        final_state_right.is_final = False

        # Add transitions from init state
        t_init_left = base.Transition(
            new_init_state, left_component_automaton.initial_state, '_')
        t_init_right = base.Transition(
            new_init_state, right_component_automaton.initial_state, '_')

        # Add transitions to final state
        t_left_final = base.Transition(final_state_left, new_final_state, '_')
        t_right_final = base.Transition(final_state_right, new_final_state, '_')

        transformed_automaton = base.AutomatonBase(alphabet=merged_alphabet)

        # Add initial and final state
        transformed_automaton.add_state(new_init_state, initial=True)
        transformed_automaton.add_state(new_final_state)

        # Add all states from left and right automaton transformations
        transformed_automaton.add_states(left_component_automaton.states.values())
        transformed_automaton.add_states(right_component_automaton.states.values())

        # Add all transitions from left and right automaton transformations
        transformed_automaton.add_transitions(
            left_component_automaton.transitions.values())
        transformed_automaton.add_transitions(
            right_component_automaton.transitions.values())

        # Add new transitions from initial and to final states
        transformed_automaton.add_transition(t_init_left)
        transformed_automaton.add_transition(t_init_right)
        transformed_automaton.add_transition(t_left_final)
        transformed_automaton.add_transition(t_right_final)

        return transformed_automaton.construct_specific_automaton()


class ConnectorKleeneStar(RegularExpressionConnector):
    max_nr_related_components = 1

    def transform(self, child_transformations):
        """
        Transform the regular expression using the Thompson algorithm.

        https://upload.wikimedia.org/wikipedia/commons/8/8e/Thompson-kleene-star.svg
        """
        child_automaton_transformation = child_transformations[0]

        child_final_state =\
            list(child_automaton_transformation.final_states.values())[0]

        if len(child_automaton_transformation.final_states.values()) > 1:
            raise model_exceptions.ModelException(
                'More than one final state found for Thompson transformation.')

        new_init_state = base.State(None, generate_state_name=True)
        new_final_state = base.State(None, is_final=True, generate_state_name=True)

        # Unset is_final status from child automaton
        child_final_state.is_final = False

        # Create tau transition from child final state to its initial state
        t_child_final_to_child_init = base.Transition(
            child_final_state, child_automaton_transformation.initial_state, '_')

        t_new_init_to_child_init = base.Transition(
            new_init_state, child_automaton_transformation.initial_state, '_')

        t_child_final_to_new_final = base.Transition(
            child_final_state, new_final_state, '_')

        t_new_init_to_new_final = base.Transition(
            new_init_state, new_final_state, '_')

        transformed_automaton = base.AutomatonBase(
            alphabet=child_automaton_transformation.alphabet)

        transformed_automaton.add_state(new_init_state, initial=True)
        transformed_automaton.add_state(new_final_state)

        transformed_automaton.add_states(
            child_automaton_transformation.states.values())

        transformed_automaton.add_transitions(
            child_automaton_transformation.transitions.values())

        transformed_automaton.add_transition(t_child_final_to_child_init)
        transformed_automaton.add_transition(t_new_init_to_child_init)
        transformed_automaton.add_transition(t_child_final_to_new_final)
        transformed_automaton.add_transition(t_new_init_to_new_final)

        return transformed_automaton.construct_specific_automaton()


REGULAR_EXPRESSION_CONNECTOR_CLASS_MAPPINGS = {
    '.': ConnectorConcatenation,
    '|': ConnectorUnion,
    '*': ConnectorKleeneStar
}

INVERTED_CLASS_CONNECTOR_MAPPINGS = {
    v.__name__: k for k, v in REGULAR_EXPRESSION_CONNECTOR_CLASS_MAPPINGS.items()}


class RegularExpression():

    def __init__(self, root_component):
        self.root_component = root_component
        self.automaton_transformation = self.root_component.convert_to_automaton()

    def get_automaton_transformation(self):
        return self.automaton_transformation

    def __repr__(self):
        return str(self.root_component)
