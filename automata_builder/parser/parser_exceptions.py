"""Holds exceptions specific to the parser module."""


class ParserException(Exception):
    def __init__(self, msg):
        self.message = msg
        super().__init__(msg)
