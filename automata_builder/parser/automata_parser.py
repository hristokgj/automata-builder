"""Module which holds automata parsing methods"""
import os
import re

from automata_builder import global_vars
from automata_builder.models import base
from automata_builder.parser import parser_exceptions as exc


def parse_automata_from_file(file_path):
    """
    Parse a file and build an automaton.
    """
    with open(file_path, mode='r', newline='\n') as f:
        string_list = []
        for line in f:
            string_list.append(line)
    automaton = base.AutomatonBase()
    parse_automata_alphabet_from_string_list(string_list, automaton)
    parse_automata_stack_alphabet_from_string_list_if_present(
        string_list, automaton)
    parse_automata_states_from_string_list(string_list, automaton)
    parse_automata_transitions_from_string_list(string_list, automaton)

    with_input_strings = check_if_input_strings_exist(string_list)
    if with_input_strings:
        parse_input_strings_from_string_list(string_list, automaton)

    return automaton.construct_specific_automaton()


def parse_automata_from_string(string):
    """
    Parse an automaton from a string.

    Assumption: new lines are separated by \\n
    """
    # XXX: Consider splitting the string_list on parts specific to each
    # parsing method as they wont have to iterate through not needed lines.
    string_list = string.split('\n')
    automaton = base.AutomatonBase()
    parse_automata_alphabet_from_string_list(string_list, automaton)
    parse_automata_stack_alphabet_from_string_list_if_present(
        string_list, automaton)
    parse_automata_states_from_string_list(string_list, automaton)
    parse_automata_transitions_from_string_list(string_list, automaton)

    with_input_strings = check_if_input_strings_exist(string_list)
    if with_input_strings:
        parse_input_strings_from_string_list(string_list, automaton)


    return automaton.construct_specific_automaton()


def parse_automata_alphabet_from_string_list(string_list, automaton):
    alphabet_parsed = False
    for s in string_list:
        s = ''.join(s.split())
        if not re.match(r'alphabet:(.)*', s, flags=re.IGNORECASE):
            continue
        split = s.split(':')[1]
        alphabet = set(list(split))
        if len(alphabet) == 0:
            raise exc.ParserException('Alphabet is empty.')

        automaton.add_alphabet(alphabet)
        alphabet_parsed = True

    if not alphabet_parsed:
        raise exc.ParserException(
            'Alphabet was not defined or incorrectly formatted..')


def parse_automata_stack_alphabet_from_string_list_if_present(
    string_list:list, automaton:base.AutomatonBase):
    for s in string_list:
        s = ''.join(s.split())
        if not re.match(r'stack:(.)*', s, flags=re.IGNORECASE):
            continue
        split = s.split(':')[1]
        stack_alphabet = set(list(split))
        if len(stack_alphabet) == 0:
            raise exc.ParserException('Stack alphabet is empty.')
        automaton.add_stack_alphabet(stack_alphabet)


def check_if_stack_alphabet_is_defined(string_list: list):
    for s in string_list:
        s = ''.join(s.split())
        if re.match(r'stack:(.)*', s, flags=re.IGNORECASE):
            return True
    return False

def parse_automata_states_from_string_list(string_list, automaton):
    states = set()
    final_states = set()
    initial_state = None
    for s in string_list:
        s = ''.join(s.split())
        parsing_states = re.match(r'states:(.)*', s, flags=re.IGNORECASE)
        parsing_final_states = re.match(r'final:(.)*', s, flags=re.IGNORECASE)
        if not parsing_states and not parsing_final_states:
            continue

        according_set = states
        if parsing_final_states:
            according_set = final_states

        s = s.split(':')
        state_list = s[1].split(',')
        if len(state_list) == 1 and state_list[0] == '':
            raise exc.ParserException("No states are specified.")

        for i, state in enumerate(state_list):
            if i == 0 and parsing_states:
                initial_state = state.upper()
            if not re.match(r'^([a-zA-Z]{1}[\d]*)+$', state):
                raise exc.ParserException(
                    f'{state} is not a valid delimiter for a state.')
            according_set.add(state.upper())

    if len(final_states - states) > 0:
        raise exc.ParserException(
            'There are final states that are not defined as states.')

    if len(final_states) == 0:
        raise exc.ParserException(
            'No final states are specified..')

    if len(states) == 0:
        raise exc.ParserException(
            'No states are specified..')
    for state_letter in states:
        is_initial = False
        is_final = False
        if state_letter == initial_state:
            is_initial = True
        if state_letter in final_states:
            is_final = True

        state = base.State(state_letter, is_final)
        automaton.add_state(state, initial=is_initial)


def parse_automata_transitions_from_string_list(string_list, automaton):
    started_transition_parsing = False
    has_stack = check_if_stack_alphabet_is_defined(string_list)
    for index, s in enumerate(string_list):
        s = ''.join(s.split())
        if re.match(r'end.', s, flags=re.IGNORECASE) and started_transition_parsing:
            break

        if started_transition_parsing:
            consumable_stack_symbol = pushable_stack_symbol = None
            source_state_name = transition_letter = None
            if re.match(r'^[\S]+,[\S]-->[\S]+$', s):
                transition_split = s.split('-->')
                source_state_name, transition_letter = transition_split[0].split(
                    ',')
                destination_state_name = transition_split[1]
                if has_stack:
                    consumable_stack_symbol = pushable_stack_symbol =\
                        base.TAU_SYMBOL
            # Stack transition
            elif re.match(r'^[\S]+,[\S]\[[\S],[\S]\]-->[\S]+$', s):
                transition_split = s.split('-->')
                source_state_name, transition_letter =\
                    transition_split[0].split('[')[0].split(',')
                consumable_stack_symbol, pushable_stack_symbol =\
                    transition_split[0].split('[')[1].split(']')[0].split(',')
                destination_state_name = transition_split[1]

            if not source_state_name or not transition_letter:
                raise exc.ParserException(
                    f'Line {index} : {s} is incorrectly formatted.')
            if(has_stack and
               (not consumable_stack_symbol or not pushable_stack_symbol)):
               raise exc.ParserException(
                   f'Line {index} : {s} is incorrectly formatted.')

            source_state = automaton.get_state(source_state_name.upper())

            destination_state = automaton.get_state(
                destination_state_name.upper())

            if not source_state or not destination_state:
                raise exc.ParserException(f'Missing state within transition:{s}')

            transition = base.Transition(
                source_state, destination_state, transition_letter)

            if has_stack:
                transition = base.StackTransition(
                    source_state, destination_state, transition_letter,
                    consumable_stack_symbol, pushable_stack_symbol)
            automaton.add_transition(transition)

        if re.match(r'transitions:(.)*', s, flags=re.IGNORECASE):
            started_transition_parsing = True

    if not started_transition_parsing:
        raise exc.ParserException(
            'Transitions were either missing or incorrectly formatted.')


def check_if_input_strings_exist(string_list):
    """
    Check if the list of strings to parse contains input strings.
    """
    for s in string_list:
        s = ''.join(s.split())
        if re.match(r'words:(.)*', s, flags=re.IGNORECASE):
            return True
    return False


def parse_input_strings_from_string_list(string_list, automaton):
    started_input_string_parsing = False
    for index, s in enumerate(string_list):
        s = ''.join(s.split())
        if(re.match(r'end.', s, flags=re.IGNORECASE) and
           started_input_string_parsing):
            break

        if started_input_string_parsing:
            try:
                input_string, expected_evaluation = s.split(',')
            except Exception:
                raise exc.ParserException(
                    f'Input String line {index}: "{s}" is incorrectly formatted.')

            if(expected_evaluation.lower() != 'y' and
               expected_evaluation.lower() != 'n'):
                raise exc.ParserException(
                    f'Input String line {index}: "{s}" is incorrectly formatted.'
                    'String acceptance is denoted with "y" or "n"')

            expected_evaluation =\
                 True if expected_evaluation.lower() == 'y' else False
            automaton.add_input_string(input_string, expected_evaluation)

        if re.match(r'words:(.)*', s, flags=re.IGNORECASE):
            started_input_string_parsing = True

    if not started_input_string_parsing:
        raise exc.ParserException(
            'Input Strings were either missing or incorrectly formatted.')
