"""Module which holds regular expression parsing methods."""

from automata_builder.models import regular_expression
from automata_builder.parser import parser_exceptions

# Remove
from automata_builder.generator import graph_generator

def parse_and_build_regular_expression(exp):
    """
    Recursively parse the regular expression string and build the model structure.

    Returns the root element for the expression.
    """
    # Remove all whitespaces
    exp = ''.join(exp.split())

    # Base Case:
    # Expression consists of one character -> it is a symbol
    if len(exp) == 1:
        if exp == regular_expression.EmptySymbol.CHARACTER:
            _class = regular_expression.EmptySymbol
        else:
            _class = regular_expression.RegularExpressionSymbol
        regular_exp_symbol = _class(exp)
        return regular_exp_symbol
    # Else it is a regular expression connector
    elif len(exp) > 1:
        connector = exp[0]
        class_entity =\
            regular_expression.REGULAR_EXPRESSION_CONNECTOR_CLASS_MAPPINGS.get(
            connector)
        if not class_entity:
            raise parser_exceptions.ParserException(
                f'Character "{connector}" is not a valid connector.')

        component = class_entity()

        inner_expressions = get_inner_expressions(exp)

        # A connector without an inner expression is invalid.
        if len(inner_expressions) == 0:
            raise parser_exceptions.ParserException('Invalid expression.')

        child_nodes = []
        for i_expression in inner_expressions:
            # Recursive call to parse the
            # inner expressions and retrieve the child nodes.
            child_nodes.append(
                parse_and_build_regular_expression(i_expression))
        component.attach_child_components(child_nodes)
        return component
    else:
        return None


def get_inner_expressions(exp):
    """
    Retrieve the inner expressions within an expression.

    Assumptions: Input is valid.
    """
    inner_expressions = []
    current_expression = ''
    opened_brackets = 1
    # Ignore the first two characters..
    # Example: |(.(a,*(b)),*(c))
    #          ^^
    for c in exp[2:]:
        if c == '(':
            opened_brackets += 1
        if c == ')':
            opened_brackets -= 1
        if (c == ',' and opened_brackets == 1) or (opened_brackets == 0):
            # Either a ',' in the top most expression
            # signifying that a new expression starts
            # or the top most expression has concluded.
            # Then the current_expression has finished and
            # can beadded to the inner_expressions.
            if current_expression != '':
                inner_expressions.append(''.join(current_expression.split()))
            current_expression = ''

        else:
            current_expression = ''.join([current_expression, c])

    return inner_expressions
