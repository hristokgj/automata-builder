"""Module which holds methods for generating graph images based on the automatas."""
import os

import graphviz

from automata_builder import global_vars
from automata_builder.models import base


def generate_graph_image_for_automaton(automaton: base.AutomatonBase):
    """
    Generate a png image of a graph for an automaton.

    """
    img_path = os.path.join(global_vars.AUTOMATA_IMAGES_DIR, automaton.id)
    dot = graphviz.Digraph(filename=img_path, format='png')

    for state in automaton.states.values():
        shape = 'circle'
        if state.is_final:
            shape = 'doublecircle'
        dot.node(state.name, shape=shape)

    # Add the transition to the initial state.
    dot.node('', shape="none")
    dot.edge('', automaton.initial_state.name)

    for transition in automaton.transitions.values():
        dot.edge(
            transition.source_state.name,
            transition.destination_state.name,
            label=transition.get_label())

    dot.render()


def generate_graph_file_for_automaton(automaton: base.AutomatonBase):
    """
    Generate a file for the automaton with the default format for the parser.

    """
    file_path = os.path.join(global_vars.AUTOMATA_FILES_DIR,
                             f'generated_{automaton.id}.aut')

    lines = []
    empty_line = '\n'

    # Add finite status and all accepted words.
    # Check as both methods may not have been implemented.
    if automaton.is_infinite() != NotImplemented:
        lines.append(f'# infinite:{automaton.is_infinite()}\n')

    all_words = automaton.get_all_words()
    if all_words != NotImplemented and len(all_words) > 0:
        lines.append(f'# Accepted words:\n')
        for word in all_words:
            lines.append(f'#\t- {word}\n')

    lines.append(empty_line)

    # Add alphabet
    alphabet_line = 'alphabet:'

    for character in automaton.alphabet:
        alphabet_line += character

    alphabet_line += '\n'
    lines.append(alphabet_line)

    lines.append(empty_line)

    if automaton.has_stack:
        stack_alphabet_line = 'stack:'
        for character in automaton.stack_alphabet:
            stack_alphabet_line += character

        stack_alphabet_line += '\n'
        lines.append(stack_alphabet_line)

        lines.append(empty_line)

    # Add states
    states_line = 'states:'
    states_line += f'{automaton.initial_state.name},'
    for state in automaton.states.values():
        if state.name == automaton.initial_state.name:
            continue
        states_line += f'{state.name},'
    # Remove last ',' character
    states_line = states_line[:-1]
    states_line += '\n'

    lines.append(states_line)

    # Add final states
    final_states_line = 'final:'
    for state in automaton.states.values():
        if not state.is_final:
            continue
        final_states_line += f'{state.name},'

    # Remove last ',' character
    final_states_line = final_states_line[:-1]
    final_states_line += '\n'
    lines.append(final_states_line)
    lines.append(empty_line)

    transitions_line_start = 'transitions:\n'
    lines.append(transitions_line_start)
    for transition in automaton.transitions.values():
        lines.append(transition.get_text_file_representation())
    transitions_line_end = 'end.\n'
    lines.append(transitions_line_end)
    lines.append(empty_line)

    if len(automaton.input_strings) > 0:
        input_words_line_start = 'words:\n'
        lines.append(input_words_line_start)
        for input_s, expected_evaluation in automaton.input_strings:
            s = f'{input_s},y\n' if expected_evaluation else f'{input_s},n\n'
            lines.append(s)
        input_words_line_end = 'end.\n'
        lines.append(input_words_line_end)

    with open(file_path, mode='w') as file:
        file.writelines(lines)
